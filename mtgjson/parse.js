'use strict';
const fs = require('fs');

/**
 * This function needs to mirror the function of the same name in /js/lib/formatter.js
 */
function cleanCardName(name) {
  return name.toLowerCase()
    .replace(/æ/g, 'ae')
    .replace(/â/g, 'a')
    .replace(/û/g, 'u')
    .replace(/ö/g, 'o')
    .replace(/á/g, 'a')
    .replace(/é/g, 'e')
    .replace(/í/g, 'i')
    .replace(/ú/g, 'u')
    .replace(/à/g, 'a')
    .replace(/®/g, '');
}


fs.readFile('AllSets.json', (err, file) => {
  if (err) {
    console.log(err);
    return;
  }
  let data = JSON.parse(file);
  let codeToName = {};
  let nameToCode = {};
  let setLists = {};
  let cardLists = {};
  let setReference = [];
  function addCode(code, name) {
    if (typeof codeToName[code] != 'undefined') {
      if (codeToName[code] != name) {
        console.log(`Collision! ${code} wants to be both ${name} and ${codeToName[code]}.`);
        codeToName[code] = 'BANNED';
        return;
      } else if (codeToName[code] == 'BANNED') {
        console.log(`Collision! ${code} also wants to be ${name}.`);
        return;
      }
    }
    codeToName[code] = name;
  }

  for (let code of Object.keys(data)) {
    let name = data[code].name;
    nameToCode[name] = code;
    nameToCode[name.toLowerCase()] = code;
    addCode(code, name);
    addCode(code.toLowerCase(), name);
    if (typeof data[code].gathererCode !== 'undefined') {
      addCode(data[code].gathererCode, name);
      addCode(data[code].gathererCode.toLowerCase(), name);
    }
    if (typeof data[code].oldCode !== 'undefined') {
      addCode(data[code].oldCode, name);
      addCode(data[code].oldCode.toLowerCase(), name);
    }
    if (typeof data[code].magicCardsInfoCode !== 'undefined') {
      codeToName[data[code].magicCardsInfoCode] = name;
      codeToName[data[code].magicCardsInfoCode.toLowerCase()] = name;
    }
    setReference.push({
      code: code,
      name: name
    });

    let setCards = data[code].cards.map(card => {
      if (typeof card.colors == 'undefined') {
        card.colors = [''];
      }

      return {
        name: card.name,
        number: card.number,
        rarity: card.rarity,
        color: card.colors.join('/')
      };
    });
    setCards.sort((a, b) => {
      const colorOrder = ['White', 'Blue', 'Black', 'Red', 'Green'];
      if (typeof a.number != 'undefined') {
        return a.number.localeCompare(b.number);
      }
      if (a.color != b.color) {
        let aColor = colorOrder.indexOf(a.color);
        let bColor = colorOrder.indexOf(b.color);
        if (aColor < 0) aColor = 99;
        if (bColor < 0) bColor = 99;
        return aColor - bColor;
      }
      return a.name.localeCompare(b.name);
    });
    setLists[code] = setCards;

    setCards.forEach(card => {
      let canonicalName = cleanCardName(card.name);
      if (typeof cardLists[canonicalName] == 'undefined') {
        cardLists[canonicalName] = {
          name: card.name,
          number: card.number,
          rarity: card.rarity,
          color: card.color,
          sets: [code]
        };
      } else {
        cardLists[canonicalName].sets.push(code);
      }
    });
  }

  // Reorder, for ease of reading
  let final = {
    codeToName: {},
    nameToCode: {}
  };
  function insensitiveSort(a, b) {
    let compare = a.toLowerCase().localeCompare(b.toLowerCase());
    if (compare === 0) {
      return a.localeCompare(b);
    }
    return compare;
  }
  Object.keys(codeToName).sort(insensitiveSort).map(key => {
    if (codeToName[key] == 'BANNED') return;
    final.codeToName[key] = codeToName[key];
  });
  Object.keys(nameToCode).sort(insensitiveSort).map(key => {
    final.nameToCode[key] = nameToCode[key];
  });
  setReference.sort((a, b) => {
    return a.name.localeCompare(b.name);
  });
  final.setReference = setReference;


  let output = "define(function() {\nreturn "+JSON.stringify(final, null, 2) + ";\n});\n";
  fs.writeFile('../web/js/lib/mtgSets.js', output, err => {
    if (err) {
      console.log(err);
    }
  });

  final = {
    bySet: setLists,
    byCard: {}
  }
  Object.keys(cardLists).sort(insensitiveSort).map(key => {
    final.byCard[key] = cardLists[key];
  });
  output = "define(function() {\nreturn "+JSON.stringify(final, null, 2) + ";\n});";
  fs.writeFile('../web/js/lib/mtgCards.js', output, err => {
    if (err) {
      console.log(err);
    }
  });

});
