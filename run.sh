#!/usr/bin/env bash

docker run -d --name magic-catalog -p 8080:80 -v "$PWD"/web:/usr/local/apache2/htdocs httpd:2.4
