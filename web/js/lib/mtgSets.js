define(function() {
return {
  "codeToName": {
    "10e": "Tenth Edition",
    "10E": "Tenth Edition",
    "15ann": "15th Anniversary",
    "1e": "Limited Edition Alpha",
    "1E": "Limited Edition Alpha",
    "2e": "Limited Edition Beta",
    "2E": "Limited Edition Beta",
    "2ed": "Unlimited Edition",
    "2ED": "Unlimited Edition",
    "2u": "Unlimited Edition",
    "2U": "Unlimited Edition",
    "3e": "Revised Edition",
    "3E": "Revised Edition",
    "3ed": "Revised Edition",
    "3ED": "Revised Edition",
    "4e": "Fourth Edition",
    "4E": "Fourth Edition",
    "4ed": "Fourth Edition",
    "4ED": "Fourth Edition",
    "5dn": "Fifth Dawn",
    "5DN": "Fifth Dawn",
    "5e": "Fifth Edition",
    "5E": "Fifth Edition",
    "5ed": "Fifth Edition",
    "5ED": "Fifth Edition",
    "6e": "Classic Sixth Edition",
    "6E": "Classic Sixth Edition",
    "6ed": "Classic Sixth Edition",
    "6ED": "Classic Sixth Edition",
    "7e": "Seventh Edition",
    "7E": "Seventh Edition",
    "7ed": "Seventh Edition",
    "7ED": "Seventh Edition",
    "8e": "Eighth Edition",
    "8E": "Eighth Edition",
    "8ed": "Eighth Edition",
    "8ED": "Eighth Edition",
    "9e": "Ninth Edition",
    "9E": "Ninth Edition",
    "9ed": "Ninth Edition",
    "9ED": "Ninth Edition",
    "ai": "Alliances",
    "AL": "Alliances",
    "ala": "Shards of Alara",
    "ALA": "Shards of Alara",
    "all": "Alliances",
    "ALL": "Alliances",
    "an": "Arabian Nights",
    "AN": "Arabian Nights",
    "ap": "Apocalypse",
    "AP": "Apocalypse",
    "apac": "Asia Pacific Land Program",
    "apc": "Apocalypse",
    "APC": "Apocalypse",
    "aq": "Antiquities",
    "AQ": "Antiquities",
    "arb": "Alara Reborn",
    "ARB": "Alara Reborn",
    "arc": "Archenemy",
    "ARC": "Archenemy",
    "arena": "Arena League",
    "arn": "Arabian Nights",
    "ARN": "Arabian Nights",
    "at": "Anthologies",
    "ath": "Anthologies",
    "ATH": "Anthologies",
    "atq": "Antiquities",
    "ATQ": "Antiquities",
    "avr": "Avacyn Restored",
    "AVR": "Avacyn Restored",
    "bd": "Beatdown Box Set",
    "BD": "Beatdown Box Set",
    "be": "Limited Edition Beta",
    "bfz": "Battle for Zendikar",
    "BFZ": "Battle for Zendikar",
    "bng": "Born of the Gods",
    "BNG": "Born of the Gods",
    "bok": "Betrayers of Kamigawa",
    "BOK": "Betrayers of Kamigawa",
    "br": "Battle Royale Box Set",
    "BR": "Battle Royale Box Set",
    "brb": "Battle Royale Box Set",
    "BRB": "Battle Royale Box Set",
    "btd": "Beatdown Box Set",
    "BTD": "Beatdown Box Set",
    "c13": "Commander 2013 Edition",
    "C13": "Commander 2013 Edition",
    "c14": "Commander 2014",
    "C14": "Commander 2014",
    "c15": "Commander 2015",
    "C15": "Commander 2015",
    "ced": "Collector's Edition",
    "CED": "Collector's Edition",
    "cedi": "International Collector's Edition",
    "cei": "International Collector's Edition",
    "CEI": "International Collector's Edition",
    "cfx": "Conflux",
    "CFX": "Conflux",
    "cg": "Urza's Destiny",
    "CG": "Urza's Destiny",
    "ch": "Chronicles",
    "CH": "Chronicles",
    "chk": "Champions of Kamigawa",
    "CHK": "Champions of Kamigawa",
    "chr": "Chronicles",
    "CHR": "Chronicles",
    "clash": "Clash Pack",
    "cm1": "Commander's Arsenal",
    "CM1": "Commander's Arsenal",
    "cma": "Commander's Arsenal",
    "CMA": "Commander's Arsenal",
    "cmd": "Magic: The Gathering-Commander",
    "CMD": "Magic: The Gathering-Commander",
    "cn2": "Conspiracy: Take the Crown",
    "CN2": "Conspiracy: Take the Crown",
    "cns": "Magic: The Gathering—Conspiracy",
    "CNS": "Magic: The Gathering—Conspiracy",
    "com": "Magic: The Gathering-Commander",
    "COM": "Magic: The Gathering-Commander",
    "con": "Conflux",
    "CON": "Conflux",
    "cp": "Champs and States",
    "cpk": "Clash Pack",
    "CPK": "Clash Pack",
    "cs": "Coldsnap",
    "csp": "Coldsnap",
    "CSP": "Coldsnap",
    "cst": "Coldsnap Theme Decks",
    "CST": "Coldsnap Theme Decks",
    "cstd": "Coldsnap Theme Decks",
    "d2": "Duel Decks: Jace vs. Chandra",
    "D2": "Duel Decks: Jace vs. Chandra",
    "dcilm": "Legend Membership",
    "dd2": "Duel Decks: Jace vs. Chandra",
    "DD2": "Duel Decks: Jace vs. Chandra",
    "dd3_dvd": "Duel Decks Anthology, Divine vs. Demonic",
    "DD3_DVD": "Duel Decks Anthology, Divine vs. Demonic",
    "dd3_evg": "Duel Decks Anthology, Elves vs. Goblins",
    "DD3_EVG": "Duel Decks Anthology, Elves vs. Goblins",
    "dd3_gvl": "Duel Decks Anthology, Garruk vs. Liliana",
    "DD3_GVL": "Duel Decks Anthology, Garruk vs. Liliana",
    "dd3_jvc": "Duel Decks Anthology, Jace vs. Chandra",
    "DD3_JVC": "Duel Decks Anthology, Jace vs. Chandra",
    "ddadvd": "Duel Decks Anthology, Divine vs. Demonic",
    "ddaevg": "Duel Decks Anthology, Elves vs. Goblins",
    "ddagvl": "Duel Decks Anthology, Garruk vs. Liliana",
    "ddajvc": "Duel Decks Anthology, Jace vs. Chandra",
    "ddc": "Duel Decks: Divine vs. Demonic",
    "DDC": "Duel Decks: Divine vs. Demonic",
    "ddd": "Duel Decks: Garruk vs. Liliana",
    "DDD": "Duel Decks: Garruk vs. Liliana",
    "dde": "Duel Decks: Phyrexia vs. the Coalition",
    "DDE": "Duel Decks: Phyrexia vs. the Coalition",
    "ddf": "Duel Decks: Elspeth vs. Tezzeret",
    "DDF": "Duel Decks: Elspeth vs. Tezzeret",
    "ddg": "Duel Decks: Knights vs. Dragons",
    "DDG": "Duel Decks: Knights vs. Dragons",
    "ddh": "Duel Decks: Ajani vs. Nicol Bolas",
    "DDH": "Duel Decks: Ajani vs. Nicol Bolas",
    "ddi": "Duel Decks: Venser vs. Koth",
    "DDI": "Duel Decks: Venser vs. Koth",
    "ddj": "Duel Decks: Izzet vs. Golgari",
    "DDJ": "Duel Decks: Izzet vs. Golgari",
    "ddk": "Duel Decks: Sorin vs. Tibalt",
    "DDK": "Duel Decks: Sorin vs. Tibalt",
    "ddl": "Duel Decks: Heroes vs. Monsters",
    "DDL": "Duel Decks: Heroes vs. Monsters",
    "ddm": "Duel Decks: Jace vs. Vraska",
    "DDM": "Duel Decks: Jace vs. Vraska",
    "ddn": "Duel Decks: Speed vs. Cunning",
    "DDN": "Duel Decks: Speed vs. Cunning",
    "ddo": "Duel Decks: Elspeth vs. Kiora",
    "DDO": "Duel Decks: Elspeth vs. Kiora",
    "ddp": "Duel Decks: Zendikar vs. Eldrazi",
    "DDP": "Duel Decks: Zendikar vs. Eldrazi",
    "ddq": "Duel Decks: Blessed vs. Cursed",
    "DDQ": "Duel Decks: Blessed vs. Cursed",
    "dgm": "Dragon's Maze",
    "DGM": "Dragon's Maze",
    "di": "Dissension",
    "dis": "Dissension",
    "DIS": "Dissension",
    "dk": "The Dark",
    "DK": "The Dark",
    "dka": "Dark Ascension",
    "DKA": "Dark Ascension",
    "dkm": "Deckmasters",
    "DKM": "Deckmasters",
    "dm": "Deckmasters",
    "dpa": "Duels of the Planeswalkers",
    "DPA": "Duels of the Planeswalkers",
    "drb": "From the Vault: Dragons",
    "DRB": "From the Vault: Dragons",
    "drc": "Dragon Con",
    "drk": "The Dark",
    "DRK": "The Dark",
    "ds": "Darksteel",
    "dst": "Darksteel",
    "DST": "Darksteel",
    "dtk": "Dragons of Tarkir",
    "DTK": "Dragons of Tarkir",
    "dvd": "Duel Decks: Divine vs. Demonic",
    "ema": "Eternal Masters",
    "EMA": "Eternal Masters",
    "emn": "Eldritch Moon",
    "EMN": "Eldritch Moon",
    "euro": "European Land Program",
    "eve": "Eventide",
    "EVE": "Eventide",
    "evg": "Duel Decks: Elves vs. Goblins",
    "EVG": "Duel Decks: Elves vs. Goblins",
    "ex": "Exodus",
    "EX": "Exodus",
    "exo": "Exodus",
    "EXO": "Exodus",
    "exp": "Zendikar Expeditions",
    "EXP": "Zendikar Expeditions",
    "fe": "Fallen Empires",
    "FE": "Fallen Empires",
    "fem": "Fallen Empires",
    "FEM": "Fallen Empires",
    "fnmp": "Friday Night Magic",
    "frf": "Fate Reforged",
    "FRF": "Fate Reforged",
    "frf_ugin": "Ugin's Fate promos",
    "FRF_UGIN": "Ugin's Fate promos",
    "fut": "Future Sight",
    "FUT": "Future Sight",
    "fvd": "From the Vault: Dragons",
    "fve": "From the Vault: Exiled",
    "FVE": "From the Vault: Exiled",
    "fvl": "From the Vault: Legends",
    "FVL": "From the Vault: Legends",
    "FVR": "From the Vault: Realms",
    "gp": "Guildpact",
    "gpt": "Guildpact",
    "GPT": "Guildpact",
    "gpx": "Grand Prix",
    "grc": "Wizards Play Network",
    "gtc": "Gatecrash",
    "GTC": "Gatecrash",
    "gu": "Urza's Legacy",
    "GU": "Urza's Legacy",
    "guru": "Guru",
    "gvl": "Duel Decks: Garruk vs. Liliana",
    "h09": "Premium Deck Series: Slivers",
    "H09": "Premium Deck Series: Slivers",
    "hho": "Happy Holidays",
    "HHO": "Happy Holidays",
    "hl": "Homelands",
    "hm": "Homelands",
    "HM": "Homelands",
    "hml": "Homelands",
    "HML": "Homelands",
    "hop": "Planechase",
    "HOP": "Planechase",
    "i2p": "Introductory Two-Player Set",
    "I2P": "Introductory Two-Player Set",
    "ia": "Ice Age",
    "IA": "Ice Age",
    "ice": "Ice Age",
    "ICE": "Ice Age",
    "in": "Invasion",
    "IN": "Invasion",
    "inv": "Invasion",
    "INV": "Invasion",
    "isd": "Innistrad",
    "ISD": "Innistrad",
    "itp": "Introductory Two-Player Set",
    "ITP": "Introductory Two-Player Set",
    "jou": "Journey into Nyx",
    "JOU": "Journey into Nyx",
    "jr": "Judge Gift Program",
    "ju": "Judgment",
    "jud": "Judgment",
    "JUD": "Judgment",
    "jvc": "Duel Decks: Jace vs. Chandra",
    "ktk": "Khans of Tarkir",
    "KTK": "Khans of Tarkir",
    "le": "Legions",
    "LE": "Legends",
    "lea": "Limited Edition Alpha",
    "LEA": "Limited Edition Alpha",
    "leb": "Limited Edition Beta",
    "LEB": "Limited Edition Beta",
    "leg": "Legends",
    "LEG": "Legends",
    "lg": "Legends",
    "lgn": "Legions",
    "LGN": "Legions",
    "lrw": "Lorwyn",
    "LRW": "Lorwyn",
    "lw": "Lorwyn",
    "m10": "Magic 2010",
    "M10": "Magic 2010",
    "m11": "Magic 2011",
    "M11": "Magic 2011",
    "m12": "Magic 2012",
    "M12": "Magic 2012",
    "m13": "Magic 2013",
    "M13": "Magic 2013",
    "m14": "Magic 2014 Core Set",
    "M14": "Magic 2014 Core Set",
    "m15": "Magic 2015 Core Set",
    "M15": "Magic 2015 Core Set",
    "mbp": "Media Inserts",
    "mbs": "Mirrodin Besieged",
    "MBS": "Mirrodin Besieged",
    "md1": "Modern Event Deck 2014",
    "MD1": "Modern Event Deck 2014",
    "me2": "Masters Edition II",
    "ME2": "Masters Edition II",
    "me3": "Masters Edition III",
    "ME3": "Masters Edition III",
    "me4": "Masters Edition IV",
    "ME4": "Masters Edition IV",
    "med": "Masters Edition",
    "MED": "Masters Edition",
    "mgb": "Multiverse Gift Box",
    "MGB": "Multiverse Gift Box",
    "mgbc": "Multiverse Gift Box",
    "mgdc": "Magic Game Day",
    "mi": "Mirrodin",
    "MI": "Mirage",
    "mir": "Mirage",
    "MIR": "Mirage",
    "mlp": "Launch Parties",
    "mm": "Mercadian Masques",
    "MM": "Mercadian Masques",
    "mm2": "Modern Masters 2015 Edition",
    "MM2": "Modern Masters 2015 Edition",
    "mma": "Modern Masters",
    "MMA": "Modern Masters",
    "mmq": "Mercadian Masques",
    "MMQ": "Mercadian Masques",
    "mor": "Morningtide",
    "MOR": "Morningtide",
    "mprp": "Magic Player Rewards",
    "mr": "Mirage",
    "mrd": "Mirrodin",
    "MRD": "Mirrodin",
    "mt": "Morningtide",
    "ne": "Nemesis",
    "NE": "Nemesis",
    "nem": "Nemesis",
    "NEM": "Nemesis",
    "nms": "Nemesis",
    "NMS": "Nemesis",
    "nph": "New Phyrexia",
    "NPH": "New Phyrexia",
    "od": "Odyssey",
    "OD": "Odyssey",
    "ody": "Odyssey",
    "ODY": "Odyssey",
    "ogw": "Oath of the Gatewatch",
    "OGW": "Oath of the Gatewatch",
    "on": "Onslaught",
    "ons": "Onslaught",
    "ONS": "Onslaught",
    "ori": "Magic Origins",
    "ORI": "Magic Origins",
    "p02": "Portal Second Age",
    "P02": "Portal Second Age",
    "p15a": "15th Anniversary",
    "p15A": "15th Anniversary",
    "p2": "Portal Second Age",
    "P2": "Portal Second Age",
    "p2hg": "Two-Headed Giant Tournament",
    "p2HG": "Two-Headed Giant Tournament",
    "p3": "Starter 1999",
    "P3": "Starter 1999",
    "p3k": "Portal Three Kingdoms",
    "p4": "Starter 2000",
    "P4": "Starter 2000",
    "palp": "Asia Pacific Land Program",
    "pALP": "Asia Pacific Land Program",
    "parl": "Arena League",
    "pARL": "Arena League",
    "pc": "Planar Chaos",
    "pc2": "Planechase 2012 Edition",
    "PC2": "Planechase 2012 Edition",
    "pcel": "Celebration",
    "pCEL": "Celebration",
    "pch": "Planechase",
    "PCH": "Planechase",
    "pcmp": "Champs and States",
    "pCMP": "Champs and States",
    "pcy": "Prophecy",
    "PCY": "Prophecy",
    "pd2": "Premium Deck Series: Fire and Lightning",
    "PD2": "Premium Deck Series: Fire and Lightning",
    "pd3": "Premium Deck Series: Graveborn",
    "PD3": "Premium Deck Series: Graveborn",
    "pdrc": "Dragon Con",
    "pDRC": "Dragon Con",
    "pds": "Premium Deck Series: Slivers",
    "pelp": "European Land Program",
    "pELP": "European Land Program",
    "pfnm": "Friday Night Magic",
    "pFNM": "Friday Night Magic",
    "pgpx": "Grand Prix",
    "pGPX": "Grand Prix",
    "pgru": "Guru",
    "pGRU": "Guru",
    "pgtw": "Gateway",
    "pGTW": "Gateway",
    "phho": "Happy Holidays",
    "pHHO": "Happy Holidays",
    "pjgp": "Judge Gift Program",
    "pJGP": "Judge Gift Program",
    "pk": "Portal Three Kingdoms",
    "PK": "Portal Three Kingdoms",
    "plc": "Planar Chaos",
    "PLC": "Planar Chaos",
    "plgm": "Legend Membership",
    "pLGM": "Legend Membership",
    "plpa": "Launch Parties",
    "pLPA": "Launch Parties",
    "pls": "Planeshift",
    "PLS": "Planeshift",
    "pmei": "Media Inserts",
    "pMEI": "Media Inserts",
    "pmgd": "Magic Game Day",
    "pMGD": "Magic Game Day",
    "pmpr": "Magic Player Rewards",
    "pMPR": "Magic Player Rewards",
    "po": "Portal",
    "PO": "Portal",
    "po2": "Portal Second Age",
    "PO2": "Portal Second Age",
    "por": "Portal",
    "POR": "Portal",
    "pot": "Portal Demo Game",
    "ppod": "Portal Demo Game",
    "pPOD": "Portal Demo Game",
    "ppre": "Prerelease Events",
    "pPRE": "Prerelease Events",
    "ppro": "Pro Tour",
    "pPRO": "Pro Tour",
    "pr": "Prophecy",
    "PR": "Prophecy",
    "prel": "Release Events",
    "pREL": "Release Events",
    "pro": "Pro Tour",
    "ps": "Planeshift",
    "PS": "Planeshift",
    "psum": "Summer of Magic",
    "pSUM": "Summer of Magic",
    "psus": "Super Series",
    "pSUS": "Super Series",
    "ptc": "Prerelease Events",
    "ptk": "Portal Three Kingdoms",
    "PTK": "Portal Three Kingdoms",
    "pvc": "Duel Decks: Phyrexia vs. the Coalition",
    "pwcq": "World Magic Cup Qualifiers",
    "pWCQ": "World Magic Cup Qualifiers",
    "pwor": "Worlds",
    "pWOR": "Worlds",
    "pwos": "Wizards of the Coast Online Store",
    "pWOS": "Wizards of the Coast Online Store",
    "pwpn": "Wizards Play Network",
    "pWPN": "Wizards Play Network",
    "rav": "Ravnica: City of Guilds",
    "RAV": "Ravnica: City of Guilds",
    "rep": "Release Events",
    "roe": "Rise of the Eldrazi",
    "ROE": "Rise of the Eldrazi",
    "rqs": "Rivals Quick Start Set",
    "RQS": "Rivals Quick Start Set",
    "rtr": "Return to Ravnica",
    "RTR": "Return to Ravnica",
    "rv": "Revised Edition",
    "s00": "Starter 2000",
    "S00": "Starter 2000",
    "s99": "Starter 1999",
    "S99": "Starter 1999",
    "sc": "Scourge",
    "scg": "Scourge",
    "SCG": "Scourge",
    "sh": "Stronghold",
    "shm": "Shadowmoor",
    "SHM": "Shadowmoor",
    "soi": "Shadows over Innistrad",
    "SOI": "Shadows over Innistrad",
    "sok": "Saviors of Kamigawa",
    "SOK": "Saviors of Kamigawa",
    "som": "Scars of Mirrodin",
    "SOM": "Scars of Mirrodin",
    "st": "Starter 1999",
    "ST": "Stronghold",
    "st2k": "Starter 2000",
    "sth": "Stronghold",
    "STH": "Stronghold",
    "sum": "Summer of Magic",
    "sus": "Super Series",
    "te": "Tempest",
    "TE": "Tempest",
    "thgt": "Two-Headed Giant Tournament",
    "ths": "Theros",
    "THS": "Theros",
    "tmp": "Tempest",
    "TMP": "Tempest",
    "tor": "Torment",
    "TOR": "Torment",
    "tp": "Tempest",
    "tpr": "Tempest Remastered",
    "TPR": "Tempest Remastered",
    "tr": "Torment",
    "ts": "Time Spiral",
    "tsb": "Time Spiral \"Timeshifted\"",
    "TSB": "Time Spiral \"Timeshifted\"",
    "tsp": "Time Spiral",
    "TSP": "Time Spiral",
    "tsts": "Time Spiral \"Timeshifted\"",
    "ud": "Urza's Destiny",
    "uds": "Urza's Destiny",
    "UDS": "Urza's Destiny",
    "ug": "Unglued",
    "UG": "Unglued",
    "ugin": "Ugin's Fate promos",
    "ugl": "Unglued",
    "UGL": "Unglued",
    "uh": "Unhinged",
    "ul": "Urza's Legacy",
    "ulg": "Urza's Legacy",
    "ULG": "Urza's Legacy",
    "un": "Unlimited Edition",
    "unh": "Unhinged",
    "UNH": "Unhinged",
    "uqc": "Celebration",
    "us": "Urza's Saga",
    "usg": "Urza's Saga",
    "USG": "Urza's Saga",
    "uz": "Urza's Saga",
    "UZ": "Urza's Saga",
    "v09": "From the Vault: Exiled",
    "V09": "From the Vault: Exiled",
    "v10": "From the Vault: Relics",
    "V10": "From the Vault: Relics",
    "v11": "From the Vault: Legends",
    "V11": "From the Vault: Legends",
    "v12": "From the Vault: Realms",
    "V12": "From the Vault: Realms",
    "v13": "From the Vault: Twenty",
    "V13": "From the Vault: Twenty",
    "v14": "From the Vault: Annihilation (2014)",
    "V14": "From the Vault: Annihilation (2014)",
    "v15": "From the Vault: Angels",
    "V15": "From the Vault: Angels",
    "v16": "From the Vault: Lore",
    "V16": "From the Vault: Lore",
    "van": "Vanguard",
    "VAN": "Vanguard",
    "vi": "Visions",
    "VI": "Visions",
    "vis": "Visions",
    "VIS": "Visions",
    "vma": "Vintage Masters",
    "VMA": "Vintage Masters",
    "w16": "Welcome Deck 2016",
    "W16": "Welcome Deck 2016",
    "wl": "Weatherlight",
    "WL": "Weatherlight",
    "wmcq": "World Magic Cup Qualifiers",
    "wotc": "Wizards of the Coast Online Store",
    "wrl": "Worlds",
    "wth": "Weatherlight",
    "WTH": "Weatherlight",
    "wwk": "Worldwake",
    "WWK": "Worldwake",
    "zen": "Zendikar",
    "ZEN": "Zendikar"
  },
  "nameToCode": {
    "15th anniversary": "p15A",
    "15th Anniversary": "p15A",
    "alara reborn": "ARB",
    "Alara Reborn": "ARB",
    "alliances": "ALL",
    "Alliances": "ALL",
    "anthologies": "ATH",
    "Anthologies": "ATH",
    "antiquities": "ATQ",
    "Antiquities": "ATQ",
    "apocalypse": "APC",
    "Apocalypse": "APC",
    "arabian nights": "ARN",
    "Arabian Nights": "ARN",
    "archenemy": "ARC",
    "Archenemy": "ARC",
    "arena league": "pARL",
    "Arena League": "pARL",
    "asia pacific land program": "pALP",
    "Asia Pacific Land Program": "pALP",
    "avacyn restored": "AVR",
    "Avacyn Restored": "AVR",
    "battle for zendikar": "BFZ",
    "Battle for Zendikar": "BFZ",
    "battle royale box set": "BRB",
    "Battle Royale Box Set": "BRB",
    "beatdown box set": "BTD",
    "Beatdown Box Set": "BTD",
    "betrayers of kamigawa": "BOK",
    "Betrayers of Kamigawa": "BOK",
    "born of the gods": "BNG",
    "Born of the Gods": "BNG",
    "celebration": "pCEL",
    "Celebration": "pCEL",
    "champions of kamigawa": "CHK",
    "Champions of Kamigawa": "CHK",
    "champs and states": "pCMP",
    "Champs and States": "pCMP",
    "chronicles": "CHR",
    "Chronicles": "CHR",
    "clash pack": "CPK",
    "Clash Pack": "CPK",
    "classic sixth edition": "6ED",
    "Classic Sixth Edition": "6ED",
    "coldsnap": "CSP",
    "Coldsnap": "CSP",
    "coldsnap theme decks": "CST",
    "Coldsnap Theme Decks": "CST",
    "collector's edition": "CED",
    "Collector's Edition": "CED",
    "commander 2013 edition": "C13",
    "Commander 2013 Edition": "C13",
    "commander 2014": "C14",
    "Commander 2014": "C14",
    "commander 2015": "C15",
    "Commander 2015": "C15",
    "commander's arsenal": "CM1",
    "Commander's Arsenal": "CM1",
    "conflux": "CON",
    "Conflux": "CON",
    "conspiracy: take the crown": "CN2",
    "Conspiracy: Take the Crown": "CN2",
    "dark ascension": "DKA",
    "Dark Ascension": "DKA",
    "darksteel": "DST",
    "Darksteel": "DST",
    "deckmasters": "DKM",
    "Deckmasters": "DKM",
    "dissension": "DIS",
    "Dissension": "DIS",
    "dragon con": "pDRC",
    "Dragon Con": "pDRC",
    "dragon's maze": "DGM",
    "Dragon's Maze": "DGM",
    "dragons of tarkir": "DTK",
    "Dragons of Tarkir": "DTK",
    "duel decks anthology, divine vs. demonic": "DD3_DVD",
    "Duel Decks Anthology, Divine vs. Demonic": "DD3_DVD",
    "duel decks anthology, elves vs. goblins": "DD3_EVG",
    "Duel Decks Anthology, Elves vs. Goblins": "DD3_EVG",
    "duel decks anthology, garruk vs. liliana": "DD3_GVL",
    "Duel Decks Anthology, Garruk vs. Liliana": "DD3_GVL",
    "duel decks anthology, jace vs. chandra": "DD3_JVC",
    "Duel Decks Anthology, Jace vs. Chandra": "DD3_JVC",
    "duel decks: ajani vs. nicol bolas": "DDH",
    "Duel Decks: Ajani vs. Nicol Bolas": "DDH",
    "duel decks: blessed vs. cursed": "DDQ",
    "Duel Decks: Blessed vs. Cursed": "DDQ",
    "duel decks: divine vs. demonic": "DDC",
    "Duel Decks: Divine vs. Demonic": "DDC",
    "duel decks: elspeth vs. kiora": "DDO",
    "Duel Decks: Elspeth vs. Kiora": "DDO",
    "duel decks: elspeth vs. tezzeret": "DDF",
    "Duel Decks: Elspeth vs. Tezzeret": "DDF",
    "duel decks: elves vs. goblins": "EVG",
    "Duel Decks: Elves vs. Goblins": "EVG",
    "duel decks: garruk vs. liliana": "DDD",
    "Duel Decks: Garruk vs. Liliana": "DDD",
    "duel decks: heroes vs. monsters": "DDL",
    "Duel Decks: Heroes vs. Monsters": "DDL",
    "duel decks: izzet vs. golgari": "DDJ",
    "Duel Decks: Izzet vs. Golgari": "DDJ",
    "duel decks: jace vs. chandra": "DD2",
    "Duel Decks: Jace vs. Chandra": "DD2",
    "duel decks: jace vs. vraska": "DDM",
    "Duel Decks: Jace vs. Vraska": "DDM",
    "duel decks: knights vs. dragons": "DDG",
    "Duel Decks: Knights vs. Dragons": "DDG",
    "duel decks: phyrexia vs. the coalition": "DDE",
    "Duel Decks: Phyrexia vs. the Coalition": "DDE",
    "duel decks: sorin vs. tibalt": "DDK",
    "Duel Decks: Sorin vs. Tibalt": "DDK",
    "duel decks: speed vs. cunning": "DDN",
    "Duel Decks: Speed vs. Cunning": "DDN",
    "duel decks: venser vs. koth": "DDI",
    "Duel Decks: Venser vs. Koth": "DDI",
    "duel decks: zendikar vs. eldrazi": "DDP",
    "Duel Decks: Zendikar vs. Eldrazi": "DDP",
    "duels of the planeswalkers": "DPA",
    "Duels of the Planeswalkers": "DPA",
    "eighth edition": "8ED",
    "Eighth Edition": "8ED",
    "eldritch moon": "EMN",
    "Eldritch Moon": "EMN",
    "eternal masters": "EMA",
    "Eternal Masters": "EMA",
    "european land program": "pELP",
    "European Land Program": "pELP",
    "eventide": "EVE",
    "Eventide": "EVE",
    "exodus": "EXO",
    "Exodus": "EXO",
    "fallen empires": "FEM",
    "Fallen Empires": "FEM",
    "fate reforged": "FRF",
    "Fate Reforged": "FRF",
    "fifth dawn": "5DN",
    "Fifth Dawn": "5DN",
    "fifth edition": "5ED",
    "Fifth Edition": "5ED",
    "fourth edition": "4ED",
    "Fourth Edition": "4ED",
    "friday night magic": "pFNM",
    "Friday Night Magic": "pFNM",
    "from the vault: angels": "V15",
    "From the Vault: Angels": "V15",
    "from the vault: annihilation (2014)": "V14",
    "From the Vault: Annihilation (2014)": "V14",
    "from the vault: dragons": "DRB",
    "From the Vault: Dragons": "DRB",
    "from the vault: exiled": "V09",
    "From the Vault: Exiled": "V09",
    "from the vault: legends": "V11",
    "From the Vault: Legends": "V11",
    "from the vault: lore": "V16",
    "From the Vault: Lore": "V16",
    "from the vault: realms": "V12",
    "From the Vault: Realms": "V12",
    "from the vault: relics": "V10",
    "From the Vault: Relics": "V10",
    "from the vault: twenty": "V13",
    "From the Vault: Twenty": "V13",
    "future sight": "FUT",
    "Future Sight": "FUT",
    "gatecrash": "GTC",
    "Gatecrash": "GTC",
    "gateway": "pGTW",
    "Gateway": "pGTW",
    "grand prix": "pGPX",
    "Grand Prix": "pGPX",
    "guildpact": "GPT",
    "Guildpact": "GPT",
    "guru": "pGRU",
    "Guru": "pGRU",
    "happy holidays": "pHHO",
    "Happy Holidays": "pHHO",
    "homelands": "HML",
    "Homelands": "HML",
    "ice age": "ICE",
    "Ice Age": "ICE",
    "innistrad": "ISD",
    "Innistrad": "ISD",
    "international collector's edition": "CEI",
    "International Collector's Edition": "CEI",
    "introductory two-player set": "ITP",
    "Introductory Two-Player Set": "ITP",
    "invasion": "INV",
    "Invasion": "INV",
    "journey into nyx": "JOU",
    "Journey into Nyx": "JOU",
    "judge gift program": "pJGP",
    "Judge Gift Program": "pJGP",
    "judgment": "JUD",
    "Judgment": "JUD",
    "khans of tarkir": "KTK",
    "Khans of Tarkir": "KTK",
    "launch parties": "pLPA",
    "Launch Parties": "pLPA",
    "legend membership": "pLGM",
    "Legend Membership": "pLGM",
    "legends": "LEG",
    "Legends": "LEG",
    "legions": "LGN",
    "Legions": "LGN",
    "limited edition alpha": "LEA",
    "Limited Edition Alpha": "LEA",
    "limited edition beta": "LEB",
    "Limited Edition Beta": "LEB",
    "lorwyn": "LRW",
    "Lorwyn": "LRW",
    "magic 2010": "M10",
    "Magic 2010": "M10",
    "magic 2011": "M11",
    "Magic 2011": "M11",
    "magic 2012": "M12",
    "Magic 2012": "M12",
    "magic 2013": "M13",
    "Magic 2013": "M13",
    "magic 2014 core set": "M14",
    "Magic 2014 Core Set": "M14",
    "magic 2015 core set": "M15",
    "Magic 2015 Core Set": "M15",
    "magic game day": "pMGD",
    "Magic Game Day": "pMGD",
    "magic origins": "ORI",
    "Magic Origins": "ORI",
    "magic player rewards": "pMPR",
    "Magic Player Rewards": "pMPR",
    "magic: the gathering-commander": "CMD",
    "Magic: The Gathering-Commander": "CMD",
    "magic: the gathering—conspiracy": "CNS",
    "Magic: The Gathering—Conspiracy": "CNS",
    "masters edition": "MED",
    "Masters Edition": "MED",
    "masters edition ii": "ME2",
    "Masters Edition II": "ME2",
    "masters edition iii": "ME3",
    "Masters Edition III": "ME3",
    "masters edition iv": "ME4",
    "Masters Edition IV": "ME4",
    "media inserts": "pMEI",
    "Media Inserts": "pMEI",
    "mercadian masques": "MMQ",
    "Mercadian Masques": "MMQ",
    "mirage": "MIR",
    "Mirage": "MIR",
    "mirrodin": "MRD",
    "Mirrodin": "MRD",
    "mirrodin besieged": "MBS",
    "Mirrodin Besieged": "MBS",
    "modern event deck 2014": "MD1",
    "Modern Event Deck 2014": "MD1",
    "modern masters": "MMA",
    "Modern Masters": "MMA",
    "modern masters 2015 edition": "MM2",
    "Modern Masters 2015 Edition": "MM2",
    "morningtide": "MOR",
    "Morningtide": "MOR",
    "multiverse gift box": "MGB",
    "Multiverse Gift Box": "MGB",
    "nemesis": "NMS",
    "Nemesis": "NMS",
    "new phyrexia": "NPH",
    "New Phyrexia": "NPH",
    "ninth edition": "9ED",
    "Ninth Edition": "9ED",
    "oath of the gatewatch": "OGW",
    "Oath of the Gatewatch": "OGW",
    "odyssey": "ODY",
    "Odyssey": "ODY",
    "onslaught": "ONS",
    "Onslaught": "ONS",
    "planar chaos": "PLC",
    "Planar Chaos": "PLC",
    "planechase": "HOP",
    "Planechase": "HOP",
    "planechase 2012 edition": "PC2",
    "Planechase 2012 Edition": "PC2",
    "planeshift": "PLS",
    "Planeshift": "PLS",
    "portal": "POR",
    "Portal": "POR",
    "portal demo game": "pPOD",
    "Portal Demo Game": "pPOD",
    "portal second age": "PO2",
    "Portal Second Age": "PO2",
    "portal three kingdoms": "PTK",
    "Portal Three Kingdoms": "PTK",
    "premium deck series: fire and lightning": "PD2",
    "Premium Deck Series: Fire and Lightning": "PD2",
    "premium deck series: graveborn": "PD3",
    "Premium Deck Series: Graveborn": "PD3",
    "premium deck series: slivers": "H09",
    "Premium Deck Series: Slivers": "H09",
    "prerelease events": "pPRE",
    "Prerelease Events": "pPRE",
    "pro tour": "pPRO",
    "Pro Tour": "pPRO",
    "prophecy": "PCY",
    "Prophecy": "PCY",
    "ravnica: city of guilds": "RAV",
    "Ravnica: City of Guilds": "RAV",
    "release events": "pREL",
    "Release Events": "pREL",
    "return to ravnica": "RTR",
    "Return to Ravnica": "RTR",
    "revised edition": "3ED",
    "Revised Edition": "3ED",
    "rise of the eldrazi": "ROE",
    "Rise of the Eldrazi": "ROE",
    "rivals quick start set": "RQS",
    "Rivals Quick Start Set": "RQS",
    "saviors of kamigawa": "SOK",
    "Saviors of Kamigawa": "SOK",
    "scars of mirrodin": "SOM",
    "Scars of Mirrodin": "SOM",
    "scourge": "SCG",
    "Scourge": "SCG",
    "seventh edition": "7ED",
    "Seventh Edition": "7ED",
    "shadowmoor": "SHM",
    "Shadowmoor": "SHM",
    "shadows over innistrad": "SOI",
    "Shadows over Innistrad": "SOI",
    "shards of alara": "ALA",
    "Shards of Alara": "ALA",
    "starter 1999": "S99",
    "Starter 1999": "S99",
    "starter 2000": "S00",
    "Starter 2000": "S00",
    "stronghold": "STH",
    "Stronghold": "STH",
    "summer of magic": "pSUM",
    "Summer of Magic": "pSUM",
    "super series": "pSUS",
    "Super Series": "pSUS",
    "tempest": "TMP",
    "Tempest": "TMP",
    "tempest remastered": "TPR",
    "Tempest Remastered": "TPR",
    "tenth edition": "10E",
    "Tenth Edition": "10E",
    "the dark": "DRK",
    "The Dark": "DRK",
    "theros": "THS",
    "Theros": "THS",
    "time spiral": "TSP",
    "Time Spiral": "TSP",
    "time spiral \"timeshifted\"": "TSB",
    "Time Spiral \"Timeshifted\"": "TSB",
    "torment": "TOR",
    "Torment": "TOR",
    "two-headed giant tournament": "p2HG",
    "Two-Headed Giant Tournament": "p2HG",
    "ugin's fate promos": "FRF_UGIN",
    "Ugin's Fate promos": "FRF_UGIN",
    "unglued": "UGL",
    "Unglued": "UGL",
    "unhinged": "UNH",
    "Unhinged": "UNH",
    "unlimited edition": "2ED",
    "Unlimited Edition": "2ED",
    "urza's destiny": "UDS",
    "Urza's Destiny": "UDS",
    "urza's legacy": "ULG",
    "Urza's Legacy": "ULG",
    "urza's saga": "USG",
    "Urza's Saga": "USG",
    "vanguard": "VAN",
    "Vanguard": "VAN",
    "vintage masters": "VMA",
    "Vintage Masters": "VMA",
    "visions": "VIS",
    "Visions": "VIS",
    "weatherlight": "WTH",
    "Weatherlight": "WTH",
    "welcome deck 2016": "W16",
    "Welcome Deck 2016": "W16",
    "wizards of the coast online store": "pWOS",
    "Wizards of the Coast Online Store": "pWOS",
    "wizards play network": "pWPN",
    "Wizards Play Network": "pWPN",
    "world magic cup qualifiers": "pWCQ",
    "World Magic Cup Qualifiers": "pWCQ",
    "worlds": "pWOR",
    "Worlds": "pWOR",
    "worldwake": "WWK",
    "Worldwake": "WWK",
    "zendikar": "ZEN",
    "Zendikar": "ZEN",
    "zendikar expeditions": "EXP",
    "Zendikar Expeditions": "EXP"
  },
  "setReference": [
    {
      "code": "p15A",
      "name": "15th Anniversary"
    },
    {
      "code": "ARB",
      "name": "Alara Reborn"
    },
    {
      "code": "ALL",
      "name": "Alliances"
    },
    {
      "code": "ATH",
      "name": "Anthologies"
    },
    {
      "code": "ATQ",
      "name": "Antiquities"
    },
    {
      "code": "APC",
      "name": "Apocalypse"
    },
    {
      "code": "ARN",
      "name": "Arabian Nights"
    },
    {
      "code": "ARC",
      "name": "Archenemy"
    },
    {
      "code": "pARL",
      "name": "Arena League"
    },
    {
      "code": "pALP",
      "name": "Asia Pacific Land Program"
    },
    {
      "code": "AVR",
      "name": "Avacyn Restored"
    },
    {
      "code": "BFZ",
      "name": "Battle for Zendikar"
    },
    {
      "code": "BRB",
      "name": "Battle Royale Box Set"
    },
    {
      "code": "BTD",
      "name": "Beatdown Box Set"
    },
    {
      "code": "BOK",
      "name": "Betrayers of Kamigawa"
    },
    {
      "code": "BNG",
      "name": "Born of the Gods"
    },
    {
      "code": "pCEL",
      "name": "Celebration"
    },
    {
      "code": "CHK",
      "name": "Champions of Kamigawa"
    },
    {
      "code": "pCMP",
      "name": "Champs and States"
    },
    {
      "code": "CHR",
      "name": "Chronicles"
    },
    {
      "code": "CPK",
      "name": "Clash Pack"
    },
    {
      "code": "6ED",
      "name": "Classic Sixth Edition"
    },
    {
      "code": "CSP",
      "name": "Coldsnap"
    },
    {
      "code": "CST",
      "name": "Coldsnap Theme Decks"
    },
    {
      "code": "CED",
      "name": "Collector's Edition"
    },
    {
      "code": "C13",
      "name": "Commander 2013 Edition"
    },
    {
      "code": "C14",
      "name": "Commander 2014"
    },
    {
      "code": "C15",
      "name": "Commander 2015"
    },
    {
      "code": "CM1",
      "name": "Commander's Arsenal"
    },
    {
      "code": "CON",
      "name": "Conflux"
    },
    {
      "code": "CN2",
      "name": "Conspiracy: Take the Crown"
    },
    {
      "code": "DKA",
      "name": "Dark Ascension"
    },
    {
      "code": "DST",
      "name": "Darksteel"
    },
    {
      "code": "DKM",
      "name": "Deckmasters"
    },
    {
      "code": "DIS",
      "name": "Dissension"
    },
    {
      "code": "pDRC",
      "name": "Dragon Con"
    },
    {
      "code": "DGM",
      "name": "Dragon's Maze"
    },
    {
      "code": "DTK",
      "name": "Dragons of Tarkir"
    },
    {
      "code": "DD3_DVD",
      "name": "Duel Decks Anthology, Divine vs. Demonic"
    },
    {
      "code": "DD3_EVG",
      "name": "Duel Decks Anthology, Elves vs. Goblins"
    },
    {
      "code": "DD3_GVL",
      "name": "Duel Decks Anthology, Garruk vs. Liliana"
    },
    {
      "code": "DD3_JVC",
      "name": "Duel Decks Anthology, Jace vs. Chandra"
    },
    {
      "code": "DDH",
      "name": "Duel Decks: Ajani vs. Nicol Bolas"
    },
    {
      "code": "DDQ",
      "name": "Duel Decks: Blessed vs. Cursed"
    },
    {
      "code": "DDC",
      "name": "Duel Decks: Divine vs. Demonic"
    },
    {
      "code": "DDO",
      "name": "Duel Decks: Elspeth vs. Kiora"
    },
    {
      "code": "DDF",
      "name": "Duel Decks: Elspeth vs. Tezzeret"
    },
    {
      "code": "EVG",
      "name": "Duel Decks: Elves vs. Goblins"
    },
    {
      "code": "DDD",
      "name": "Duel Decks: Garruk vs. Liliana"
    },
    {
      "code": "DDL",
      "name": "Duel Decks: Heroes vs. Monsters"
    },
    {
      "code": "DDJ",
      "name": "Duel Decks: Izzet vs. Golgari"
    },
    {
      "code": "DD2",
      "name": "Duel Decks: Jace vs. Chandra"
    },
    {
      "code": "DDM",
      "name": "Duel Decks: Jace vs. Vraska"
    },
    {
      "code": "DDG",
      "name": "Duel Decks: Knights vs. Dragons"
    },
    {
      "code": "DDE",
      "name": "Duel Decks: Phyrexia vs. the Coalition"
    },
    {
      "code": "DDK",
      "name": "Duel Decks: Sorin vs. Tibalt"
    },
    {
      "code": "DDN",
      "name": "Duel Decks: Speed vs. Cunning"
    },
    {
      "code": "DDI",
      "name": "Duel Decks: Venser vs. Koth"
    },
    {
      "code": "DDP",
      "name": "Duel Decks: Zendikar vs. Eldrazi"
    },
    {
      "code": "DPA",
      "name": "Duels of the Planeswalkers"
    },
    {
      "code": "8ED",
      "name": "Eighth Edition"
    },
    {
      "code": "EMN",
      "name": "Eldritch Moon"
    },
    {
      "code": "EMA",
      "name": "Eternal Masters"
    },
    {
      "code": "pELP",
      "name": "European Land Program"
    },
    {
      "code": "EVE",
      "name": "Eventide"
    },
    {
      "code": "EXO",
      "name": "Exodus"
    },
    {
      "code": "FEM",
      "name": "Fallen Empires"
    },
    {
      "code": "FRF",
      "name": "Fate Reforged"
    },
    {
      "code": "5DN",
      "name": "Fifth Dawn"
    },
    {
      "code": "5ED",
      "name": "Fifth Edition"
    },
    {
      "code": "4ED",
      "name": "Fourth Edition"
    },
    {
      "code": "pFNM",
      "name": "Friday Night Magic"
    },
    {
      "code": "V15",
      "name": "From the Vault: Angels"
    },
    {
      "code": "V14",
      "name": "From the Vault: Annihilation (2014)"
    },
    {
      "code": "DRB",
      "name": "From the Vault: Dragons"
    },
    {
      "code": "V09",
      "name": "From the Vault: Exiled"
    },
    {
      "code": "V11",
      "name": "From the Vault: Legends"
    },
    {
      "code": "V16",
      "name": "From the Vault: Lore"
    },
    {
      "code": "V12",
      "name": "From the Vault: Realms"
    },
    {
      "code": "V10",
      "name": "From the Vault: Relics"
    },
    {
      "code": "V13",
      "name": "From the Vault: Twenty"
    },
    {
      "code": "FUT",
      "name": "Future Sight"
    },
    {
      "code": "GTC",
      "name": "Gatecrash"
    },
    {
      "code": "pGTW",
      "name": "Gateway"
    },
    {
      "code": "pGPX",
      "name": "Grand Prix"
    },
    {
      "code": "GPT",
      "name": "Guildpact"
    },
    {
      "code": "pGRU",
      "name": "Guru"
    },
    {
      "code": "pHHO",
      "name": "Happy Holidays"
    },
    {
      "code": "HML",
      "name": "Homelands"
    },
    {
      "code": "ICE",
      "name": "Ice Age"
    },
    {
      "code": "ISD",
      "name": "Innistrad"
    },
    {
      "code": "CEI",
      "name": "International Collector's Edition"
    },
    {
      "code": "ITP",
      "name": "Introductory Two-Player Set"
    },
    {
      "code": "INV",
      "name": "Invasion"
    },
    {
      "code": "JOU",
      "name": "Journey into Nyx"
    },
    {
      "code": "pJGP",
      "name": "Judge Gift Program"
    },
    {
      "code": "JUD",
      "name": "Judgment"
    },
    {
      "code": "KTK",
      "name": "Khans of Tarkir"
    },
    {
      "code": "pLPA",
      "name": "Launch Parties"
    },
    {
      "code": "pLGM",
      "name": "Legend Membership"
    },
    {
      "code": "LEG",
      "name": "Legends"
    },
    {
      "code": "LGN",
      "name": "Legions"
    },
    {
      "code": "LEA",
      "name": "Limited Edition Alpha"
    },
    {
      "code": "LEB",
      "name": "Limited Edition Beta"
    },
    {
      "code": "LRW",
      "name": "Lorwyn"
    },
    {
      "code": "M10",
      "name": "Magic 2010"
    },
    {
      "code": "M11",
      "name": "Magic 2011"
    },
    {
      "code": "M12",
      "name": "Magic 2012"
    },
    {
      "code": "M13",
      "name": "Magic 2013"
    },
    {
      "code": "M14",
      "name": "Magic 2014 Core Set"
    },
    {
      "code": "M15",
      "name": "Magic 2015 Core Set"
    },
    {
      "code": "pMGD",
      "name": "Magic Game Day"
    },
    {
      "code": "ORI",
      "name": "Magic Origins"
    },
    {
      "code": "pMPR",
      "name": "Magic Player Rewards"
    },
    {
      "code": "CMD",
      "name": "Magic: The Gathering-Commander"
    },
    {
      "code": "CNS",
      "name": "Magic: The Gathering—Conspiracy"
    },
    {
      "code": "MED",
      "name": "Masters Edition"
    },
    {
      "code": "ME2",
      "name": "Masters Edition II"
    },
    {
      "code": "ME3",
      "name": "Masters Edition III"
    },
    {
      "code": "ME4",
      "name": "Masters Edition IV"
    },
    {
      "code": "pMEI",
      "name": "Media Inserts"
    },
    {
      "code": "MMQ",
      "name": "Mercadian Masques"
    },
    {
      "code": "MIR",
      "name": "Mirage"
    },
    {
      "code": "MRD",
      "name": "Mirrodin"
    },
    {
      "code": "MBS",
      "name": "Mirrodin Besieged"
    },
    {
      "code": "MD1",
      "name": "Modern Event Deck 2014"
    },
    {
      "code": "MMA",
      "name": "Modern Masters"
    },
    {
      "code": "MM2",
      "name": "Modern Masters 2015 Edition"
    },
    {
      "code": "MOR",
      "name": "Morningtide"
    },
    {
      "code": "MGB",
      "name": "Multiverse Gift Box"
    },
    {
      "code": "NMS",
      "name": "Nemesis"
    },
    {
      "code": "NPH",
      "name": "New Phyrexia"
    },
    {
      "code": "9ED",
      "name": "Ninth Edition"
    },
    {
      "code": "OGW",
      "name": "Oath of the Gatewatch"
    },
    {
      "code": "ODY",
      "name": "Odyssey"
    },
    {
      "code": "ONS",
      "name": "Onslaught"
    },
    {
      "code": "PLC",
      "name": "Planar Chaos"
    },
    {
      "code": "HOP",
      "name": "Planechase"
    },
    {
      "code": "PC2",
      "name": "Planechase 2012 Edition"
    },
    {
      "code": "PLS",
      "name": "Planeshift"
    },
    {
      "code": "POR",
      "name": "Portal"
    },
    {
      "code": "pPOD",
      "name": "Portal Demo Game"
    },
    {
      "code": "PO2",
      "name": "Portal Second Age"
    },
    {
      "code": "PTK",
      "name": "Portal Three Kingdoms"
    },
    {
      "code": "PD2",
      "name": "Premium Deck Series: Fire and Lightning"
    },
    {
      "code": "PD3",
      "name": "Premium Deck Series: Graveborn"
    },
    {
      "code": "H09",
      "name": "Premium Deck Series: Slivers"
    },
    {
      "code": "pPRE",
      "name": "Prerelease Events"
    },
    {
      "code": "pPRO",
      "name": "Pro Tour"
    },
    {
      "code": "PCY",
      "name": "Prophecy"
    },
    {
      "code": "RAV",
      "name": "Ravnica: City of Guilds"
    },
    {
      "code": "pREL",
      "name": "Release Events"
    },
    {
      "code": "RTR",
      "name": "Return to Ravnica"
    },
    {
      "code": "3ED",
      "name": "Revised Edition"
    },
    {
      "code": "ROE",
      "name": "Rise of the Eldrazi"
    },
    {
      "code": "RQS",
      "name": "Rivals Quick Start Set"
    },
    {
      "code": "SOK",
      "name": "Saviors of Kamigawa"
    },
    {
      "code": "SOM",
      "name": "Scars of Mirrodin"
    },
    {
      "code": "SCG",
      "name": "Scourge"
    },
    {
      "code": "7ED",
      "name": "Seventh Edition"
    },
    {
      "code": "SHM",
      "name": "Shadowmoor"
    },
    {
      "code": "SOI",
      "name": "Shadows over Innistrad"
    },
    {
      "code": "ALA",
      "name": "Shards of Alara"
    },
    {
      "code": "S99",
      "name": "Starter 1999"
    },
    {
      "code": "S00",
      "name": "Starter 2000"
    },
    {
      "code": "STH",
      "name": "Stronghold"
    },
    {
      "code": "pSUM",
      "name": "Summer of Magic"
    },
    {
      "code": "pSUS",
      "name": "Super Series"
    },
    {
      "code": "TMP",
      "name": "Tempest"
    },
    {
      "code": "TPR",
      "name": "Tempest Remastered"
    },
    {
      "code": "10E",
      "name": "Tenth Edition"
    },
    {
      "code": "DRK",
      "name": "The Dark"
    },
    {
      "code": "THS",
      "name": "Theros"
    },
    {
      "code": "TSP",
      "name": "Time Spiral"
    },
    {
      "code": "TSB",
      "name": "Time Spiral \"Timeshifted\""
    },
    {
      "code": "TOR",
      "name": "Torment"
    },
    {
      "code": "p2HG",
      "name": "Two-Headed Giant Tournament"
    },
    {
      "code": "FRF_UGIN",
      "name": "Ugin's Fate promos"
    },
    {
      "code": "UGL",
      "name": "Unglued"
    },
    {
      "code": "UNH",
      "name": "Unhinged"
    },
    {
      "code": "2ED",
      "name": "Unlimited Edition"
    },
    {
      "code": "UDS",
      "name": "Urza's Destiny"
    },
    {
      "code": "ULG",
      "name": "Urza's Legacy"
    },
    {
      "code": "USG",
      "name": "Urza's Saga"
    },
    {
      "code": "VAN",
      "name": "Vanguard"
    },
    {
      "code": "VMA",
      "name": "Vintage Masters"
    },
    {
      "code": "VIS",
      "name": "Visions"
    },
    {
      "code": "WTH",
      "name": "Weatherlight"
    },
    {
      "code": "W16",
      "name": "Welcome Deck 2016"
    },
    {
      "code": "pWOS",
      "name": "Wizards of the Coast Online Store"
    },
    {
      "code": "pWPN",
      "name": "Wizards Play Network"
    },
    {
      "code": "pWCQ",
      "name": "World Magic Cup Qualifiers"
    },
    {
      "code": "pWOR",
      "name": "Worlds"
    },
    {
      "code": "WWK",
      "name": "Worldwake"
    },
    {
      "code": "ZEN",
      "name": "Zendikar"
    },
    {
      "code": "EXP",
      "name": "Zendikar Expeditions"
    }
  ]
};
});
