# Magic: the Gathering Collection
Simple collection management tool. It uses PouchDB to create a local database of cards, so each computer (and each browser) you open it on will have a different collection if you use it that way.

You can connect to a centralized CouchDB server in order to keep your collection in sync across devices, if you'd like.

# Run locally
The contents of the `web/` folder contains the static files needed to run the application. However, to make sure the local web databases work properly, they should be launched with a web server, rather than opened directly.

In a docker environment, run:

```bash
docker run -d --name magic-catalog -p 8080:80 -v "$PWD"/web:/usr/local/apache2/htdocs httpd:2.4
```

And then visit `http://localhost:8000` to view the application.

# Develop
Install the required Node modules by running

```bash
npm install
```

inside the project directory. If developing in a Docker environment, where Node/NPM isn't installed locally, run:

```bash
docker run -it --rm --name npm -v "$PWD":/usr/src/app -w /usr/src/app node npm install --no-bin-links
```

# Build
This project uses ES2016 Javascript syntax, with React components written in JSX, which needs to be compiled down for common web use.

Gulp is used as a build tool; if you don't have Node installed locally, you can use a Docker environment to create a container that can run the Gulp tasks:

```bash
docker build -f Dockerfile-gulp --tag gulp ./
docker run -it --rm --name gulp -v "$PWD":/usr/src/app -w /usr/src/app gulp bash
```
## Update expansion sets
The set information from [mtgjson](https://mtgjson.com/) is used to do mapping in this application. When a new expansion is released, the set mapping needs to be updated:

```bash
cd mtgjson
wget https://mtgjson.com/json/AllSets.json.zip
unzip AllSets.json.zip
node parse.js
```

The `parse.js` script will extract out just the bits of information this application needs, and save it to `js/lib/mtgSets.js`, ready to be built into the app.

## Compile LESS

```bash
gulp less
```

## Compile Javascript
For development use, you can compile the javascript to include source-maps, which make browser console debugging easier:

```bash
gulp es2015-map
```

For compiling a final build (to check into the repository), remove the sourcemaps for less size:

```bash
gulp es2015
```
