import PouchDB from 'pouch';
import mtgCards from 'lib/mtgCards';

export default {
  connect: () => {
    return new PouchDB('magic-collection');
  },

  upsert: (db, key, value) => {
    return db.get(key).then(doc => {
      let updated = Object.assign({}, value);
      updated._rev = doc._rev;
      updated._id = key;
      return db.put(updated);
    }, err => {
      if (err.status == 404) {
        // No current doc; just put
        let insert = Object.assign({}, value);
        insert._id = key;
        return db.put(insert);
      }
      throw err; // Some other sort of error; re-throw
    });
  },

  countCards: (records) => {
    return records.reduce((current, rs) => {
      let doc;
      if (typeof rs.doc != 'undefined') {
        doc = rs.doc;
      } else {
        doc = rs;
      }
      if (typeof doc.quantity == 'undefined') {
        return current+1;
      }
      return current + parseInt(doc.quantity);
    }, 0);
  },

  mergeCards: (records) => {
    let out = {};
    records.map(record => {
      if (typeof out[record.name] == 'undefined') {
        out[record.name] = {
          name: record.name,
          color: record.color,
          quantity: parseInt(record.quantity)
        };
      } else {
        out[record.name].quantity += parseInt(record.quantity);
      }
    });
    let final = [];
    Object.keys(out).map(key => {
      final.push(out[key]);
    });
    return final;
  },

  findById: (records, id) => {
    for (let i = 0; i < records.length; i++) {
      if (records[i]._id == id) {
        return records[i];
      }
    }
    return false;
  },

  setColorRarity: (card) => {
    if (typeof card.expansion == 'undefined') {
      console.error('No expansion set; cannot set rarity');
      return card;
    }
    let expansionCode = card.expansion.code;
    if (typeof mtgCards.bySet[expansionCode] == 'undefined') {
      console.error('No expansion data for ' + expansionCode);
      return card;
    }
    for (let cardData of mtgCards.bySet[expansionCode]) {
      if (cardData.name == card.name) {
        card.rarity = cardData.rarity;
        card.color = cardData.color;
        return card;
      }
    }
    console.error('No card data found for ' + card.name + ' in ' + card.expansion.code);
    return card;
  }
};
