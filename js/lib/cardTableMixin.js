export default {
  filterCards: function(cards, filterString) {
    return cards.filter(card => {
      if (card.name.toLowerCase().indexOf(filterString) >= 0) {
        return true;
      }
      return false;
    });
  },
  sortCards: function(cards, field, direction) {
    let colorRank = {
      'White': 0,
      'Blue': 1,
      'Black': 2,
      'Red': 3,
      'Green': 4
    };
    let rarityRank = {
      'Common': 0,
      'Uncommon': 1,
      'Rare': 2,
      'Mythic': 3,
      'Promo': 4,
      'Land': 5
    };
    return cards.sort((a, b) => {
      if (field == 'name') {
        if (direction == 'asc') {
          return a.name.localeCompare(b.name);
        } else {
          return b.name.localeCompare(a.name);
        }
      } else if (field == 'quantity') {
        if (a.quantity == b.quantity) {
          return a.name.localeCompare(b.name);
        }
        if (direction == 'asc') {
          return a.quantity - b.quantity;
        } else {
          return b.quantity - a.quantity;
        }
      } else if (field == 'foil') {
        if (direction == 'asc') {
          if (!b.foil && a.foil) {
            return -1;
          } else if (!a.foil && b.foil) {
            return 1;
          } else {
            return a.name.localeCompare(b.name);
          }
        } else {
          if (!a.foil && b.foil) {
            return -1;
          } else if (!b.foil && a.foil) {
            return 1;
          } else {
            return a.name.localeCompare(b.name);
          }
        }
      } else if (field == 'set') {
        if (typeof a.expansion == 'undefined' && typeof b.expansion != 'undefined') {
          return direction == 'asc' ? -1 : 1;
        } else if (typeof b.expansion == 'undefined' && typeof a.expansion != 'undefined') {
          return direction == 'asc' ? 1 : -1;
        } else if (typeof a.expansion == 'undefined' && typeof b.expansion == 'undefined') {
          return a.name.localeCompare(b.name);
        } else if (a.expansion.name == b.expansion.name) {
          return a.name.localeCompare(b.name);
        }
        if (direction == 'asc') {
          return a.expansion.name.localeCompare(b.expansion.name);
        } else {
          return b.expansion.name.localeCompare(a.expansion.name);
        }
      } else if (field == 'color') {
        let aColor = a.color;
        let bColor = b.color;
        if (typeof aColor == 'undefined') aColor = '';
        if (typeof bColor == 'undefined') bColor = '';
        if (aColor == '' && bColor != '') {
          return direction == 'asc' ? 1 : -1;
        } else if (bColor == '' && aColor != '') {
          return direction == 'asc' ? -1 : 1;
        } else if (aColor == '' && bColor == '') {
          return a.name.localeCompare(b.name);
        } else if (aColor == bColor) {
          return a.name.localeCompare(b.name);
        } else if (typeof colorRank[aColor] == 'undefined' && typeof colorRank[bColor] != 'undefined') {
          return direction == 'asc' ? 1 : -1;
        } else if (typeof colorRank[bColor] == 'undefined' && typeof colorRank[aColor] != 'undefined') {
          return direction == 'asc' ? -1 : 1;
        } else if (typeof colorRank[aColor] == 'undefined' && typeof colorRank[bColor] == 'undefined') {
          return a.name.localeCompare(b.name);
        }
        if (direction == 'asc') {
          return colorRank[aColor] - colorRank[bColor];
        } else {
          return colorRank[bColor] - colorRank[aColor];
        }
      } else if (field == 'rarity') {
        let aRarity = a.rarity;
        let bRarity = b.rarity;
        if (typeof aRarity == 'undefined') aRarity = '';
        if (typeof bRarity == 'undefined') bRarity = '';
        if (aRarity == '' && bRarity != '') {
          return direction == 'asc' ? -1 : 1;
        } else if (bRarity == '' && aRarity != '') {
          return direction == 'asc' ? 1 : -1;
        } else if (aRarity == '' && bRarity == '') {
          return a.name.localeCompare(b.name);
        } else if (aRarity == bRarity) {
          return a.name.localeCompare(b.name);
        }
        if (direction == 'asc') {
          return rarityRank[aRarity] - rarityRank[bRarity];
        } else {
          return rarityRank[bRarity] - rarityRank[aRarity];
        }

      }
    });
  },
  getDefaultDirection: function(sortType) {
    switch (sortType) {
      case 'quantity':
        return 'desc';
    }
    return 'asc';
  },
  getNextPage: function (currentPage, pageSize, recordCount) {
    let newPage = currentPage+1;
    if ((newPage-1) * pageSize > recordCount) {
      return currentPage;
    }
    return newPage;
  },
  getPreviousPage: function (currentPage) {
    let newPage = currentPage-1;
    if (newPage < 1) return currentPage;
    return newPage;
  },
  getCurrentPageRange: function(currentPage, pageSize) {
    let startAt = (currentPage - 1) * pageSize;
    let endAt = startAt + pageSize;
    return [startAt, endAt];
  }

};
