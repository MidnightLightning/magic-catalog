import Tokenizer from 'lib/tokenizer';

/**
 * Tokenizer to separate out brace/bracket/parens groupings
 * Foil marker has its own specific match, while Expansion set matching could be several things
 */
const tokenizer = new Tokenizer([
  {
    regex: /[([{]F[)\]}]/i,
    name: 'FOIL_MARKER'
  },
  {
    regex: /\/\//,
    name: 'DOUBLE_SLASH'
  },
  {
    regex: /[0-9]+/,
    name: 'DIGIT'
  },
  {
    regex: /[\n\r]+/,
    name: 'NEWLINE'
  },
  {
    regex: /\{/,
    name: 'OPEN_CURLY_BRACKET'
  },
  {
    regex: /\}/,
    name: 'CLOSE_CURLY_BRACKET'
  },
  {
    regex: /\[/,
    name: 'OPEN_BRACKET'
  },
  {
    regex: /\]/,
    name: 'CLOSE_BRACKET'
  },
  {
    regex: /\(/,
    name: 'OPEN_PARENS'
  },
  {
    regex: /\)/,
    name: 'CLOSE_PARENS'
  },
  {
    regex: /:/,
    name: 'COLON'
  },
  {
    regex: /[a-zA-Z\-_]+/,
    name: 'ALPHA_TEXT'
  },
  {
    regex: /\s+/,
    name: 'WHITESPACE'
  },
  {
    regex: /\S/,
    name: 'CHARACTER'
  }
]);

export default function lex(string) {
  if (string == '') {
    return [];
  }
  return tokenizer.parseText(string);
}
