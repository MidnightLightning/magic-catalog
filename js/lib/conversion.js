import mtgSets from 'lib/mtgSets';

export default {

  /**
   * Debug token structure
   */
  printTokens: tokens => {
    console.debug(tokens.map(token => {
      return `${token.type}(${token.value})`;
    }));
  },

  /**
   * Slice an array of tokens on each newline found
   */
  splitOnNewline: tokens => {
    let output = [];
    // Ensure no newline in front
    let lastNewline = false;
    for (let index = 0; index < tokens.length; index++) {
      if (tokens[index].type !== 'NEWLINE') {
        lastNewline = index-1;
        break;
      }
    }

    for (let index = lastNewline+1; index < tokens.length; index++) {
      if (tokens[index].type == 'NEWLINE') {
        output.push(tokens.slice(lastNewline+1, index));
        lastNewline = index;
      }
    }

    // Add the last chunk of tokens
    if (tokens[tokens.length-1].type !== 'NEWLINE') {
      output.push(tokens.slice(lastNewline+1));
    }
    return output;
  },

  /**
   * Parse a list of tokens:
   * Look for opening braces/brackets and extract the grouped tokens
   */
  parseGroups: function parseGroups(tokens) {
    let out = [];
    let index = 0;
    const openings = [
      'OPEN_CURLY_BRACKET',
      'OPEN_BRACKET',
      'OPEN_PARENS'
    ];
    const closings = [
      'CLOSE_CURLY_BRACKET',
      'CLOSE_BRACKET',
      'CLOSE_PARENS'
    ];
    while (true) {
      let currentToken = tokens[index];
      if (openings.indexOf(currentToken.type) >= 0) {
        // Beginning of group expression
        let groupLevel = 1;
        let foundClosing = false;
        for (let j = index+1; j < tokens.length; j++) {
          if (closings.indexOf(tokens[j].type) >= 0) {
            groupLevel--;
          } else if (openings.indexOf(tokens[j].type) >= 0) {
            groupLevel++;
          }
          if (groupLevel == 0) {
            // Found the end of this group
            let children = tokens.slice(index+1, j);
            children = parseGroups(children);
            out.push({
              type: 'GROUP',
              value: children.map(token => token.value).join('')
            });

            foundClosing = true;
            index = j;
            j = tokens.length+10; // Break out of this loop
          }
        }
        if (foundClosing === false) {
          // No closing bracket? Treat as text instead
          out.push(currentToken);
        }
      } else {
        // Not a grouping
        out.push(currentToken);
      }
      index++;
      if (index >= tokens.length) {
        return out;
      }
    }
  },

  /**
   * Parse a list of tokens:
   * Remove whitespace before/after GROUP tokens
   */
  stripWhitespace: tokens => {
    let out = [];
    let index = 0;
    const groups = [
      'GROUP',
      'FOIL_MARKER'
    ];
    while (true) {
      let currentToken = tokens[index];
      if (currentToken.type == 'WHITESPACE') {
        if (groups.indexOf(tokens[index+1].type) >= 0
          || groups.indexOf(tokens[index-1].type) >= 0
        ) {
          // Skip
        } else {
          out.push(currentToken);
        }
      } else {
        out.push(currentToken);
      }
      index++;
      if (index >= tokens.length) {
        return out;
      }
    }
  },

  /**
   * Parse a list of tokens:
   * Remove GROUP tokens
   */
  stripGroups: tokens => {
    const remove = [
      'GROUP',
      'FOIL_MARKER'
    ];
    return tokens.filter(token => {
      return remove.indexOf(token.type) < 0;
    });
  },

  extractGroups: tokens => {
    const keep = [
      'GROUP',
      'FOIL_MARKER'
    ];
    return tokens.filter(token => {
      return keep.indexOf(token.type) >= 0;
    });
  },

  fromDecklist: function fromDecklist(tokens) {
    if (tokens.length == 0) {
      return false;
    } else if (tokens[0].type == 'DOUBLE_SLASH') {
      return false; // Comment line
    }
    let countNonWhitespace = tokens.reduce((current, token) => {
      if (token.type != 'WHITESPACE') {
        return current+1;
      }
      return current;
    }, 0);
    if (countNonWhitespace == 0) {
      return false; // Blank line
    }

    let output = {};
    let startAt = 0;
    for (let index = 0; index < tokens.length; index++) {
      if (tokens[index].type !== 'WHITESPACE') {
        startAt = index;
        break;
      }
    }

    // Starts with a digit?
    if (tokens[startAt].type == 'DIGIT') {
      output.quantity = tokens[startAt].value;
    } else {
      output.quantity = 1;
    }

    // Skip any whitespace after the quantity
    for (let index = startAt+1; index < tokens.length; index++) {
      if (tokens[index].type !== 'WHITESPACE') {
        startAt = index;
        break;
      }
    }

    let label = tokens.slice(startAt);
    label = this.parseGroups(label);
    label = this.stripWhitespace(label);

    output.name = this.stripGroups(label).map(token => token.value).join('');
    let groups = this.extractGroups(label);
    groups.map(group => {
      let groupName = group.value.trim().toLowerCase();
      if (group.type == 'FOIL_MARKER' || groupName == 'f') {
        output.foil = true;
        return;
      }
      if (typeof mtgSets.nameToCode[groupName] !== 'undefined') {
        let code = mtgSets.nameToCode[groupName];
        let name = mtgSets.codeToName[code]; // Ensure we have proper-cased name
        output.expansion = {
          code,
          name
        };
        return;
      }
      if (typeof mtgSets.codeToName[groupName] !== 'undefined') {
        let name = mtgSets.codeToName[groupName];
        let code = mtgSets.nameToCode[name]; // Ensure we have proper-cased code
        output.expansion = {
          code,
          name
        };
        return;
      }

      // Unknown group...
      if (typeof output.groups == 'undefined') {
        output.groups = [];
      }
      output.groups.push(groupName);
    });

    return output;
  }

};
