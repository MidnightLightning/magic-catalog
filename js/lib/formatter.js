import React from 'react';
import mtgCards from 'lib/mtgCards';

export default {
  interleave: function(arr, glue) {
    if (!Array.isArray(arr)) {
      return [arr];
    }
    let out = [];
    for (let i = 0; i < arr.length; i++) {
      out.push(arr[i]);
      if (i !== arr.length-1) {
        out.push(glue);
      }
    }
    return out;
  },
  setOptions: function(setList) {
    return setList.map(set => {
      return (
        <option
          key={set.code}
          value={set.code}
        >
          {set.name} ({set.code})
        </option>
      );
    });
  },
  setsWithCard: function(cardName, allSets) {
    if (typeof cardName == 'undefined' || cardName.trim() == '') return allSets;
    let canonicalName = this.cleanCardName(cardName);
    let cardData = mtgCards.byCard[canonicalName];
    if (typeof cardData == 'undefined') return allSets;
    return allSets.filter(set => {
      return cardData.sets.indexOf(set.code) >= 0;
    });
  },
  cleanCardName: function(name) {
    return name.toLowerCase()
      .replace(/æ/g, 'ae')
      .replace(/â/g, 'a')
      .replace(/û/g, 'u')
      .replace(/ö/g, 'o')
      .replace(/á/g, 'a')
      .replace(/é/g, 'e')
      .replace(/í/g, 'i')
      .replace(/ú/g, 'u')
      .replace(/à/g, 'a')
      .replace(/®/g, '');
  }

};
