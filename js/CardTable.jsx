import React from 'react';
import dbUtil from './lib/dbUtil';
import rows from './CardTableRow';

class CardTable extends React.Component {
  constructor(props) {
    super(props);
    let sortField;
    let sortDirection;
    if (typeof props.sorted != 'undefined') {
      sortField = props.sorted;
      sortDirection = this.getDefaultDirection(sortField);
    }
    let pageSize = 20;
    if (props.pagination === false) {
      pageSize = 99999999;
    } else if (typeof props.pageSize != 'undefined') {
      pageSize = parseInt(props.pageSize);
    }
    let cards = [];
    if (typeof props.cards != 'undefined') {
      cards = props.cards;
    }
    this.state = {
      sortField: sortField,
      sortDirection: sortDirection,
      cards: cards,
      cardFilter: '',
      selectedCards: {},
      pageSize: pageSize,
      currentPage: 1
    };

    this.defaultColumns = [
      'quantity',
      'name',
      'foil',
      'expansion',
      'color',
      'rarity'
    ];
    this.handleFilter = this.handleFilter.bind(this);
    this.handleSort = this.handleSort.bind(this);
    this.pageNext = this.pageNext.bind(this);
    this.pagePrevious = this.pagePrevious.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    let filtered = this.filterCards(nextProps.cards, this.state.cardFilter);
    this.setState({
      cards: this.sortCards(filtered, this.state.sortField, this.state.sortDirection),
    });
  }
  filterCards(cards, filterString) {
    if (filterString == '') {
      return cards;
    }
    return cards.filter(card => {
      if (card.name.toLowerCase().indexOf(filterString) >= 0) {
        return true;
      }
      return false;
    });
  }
  handleFilter(el) {
    let searchFilter = el.target.value.toLowerCase();
    let filtered = this.filterCards(this.props.cards, searchFilter);
    this.setState({
      cardFilter: searchFilter,
      cards: this.sortCards(filtered, this.state.sortField, this.state.sortDirection),
      currentPage: 1
    });
  }
  sortCards(cards, field, direction) {
    if (typeof field == 'undefined') return cards; // No sorting
    let colorRank = {
      'White': 0,
      'Blue': 1,
      'Black': 2,
      'Red': 3,
      'Green': 4
    };
    let rarityRank = {
      'Common': 0,
      'Uncommon': 1,
      'Rare': 2,
      'Mythic': 3,
      'Promo': 4,
      'Land': 5
    };
    switch(field) {
      case 'name':
        if (direction == 'asc') {
          return cards.sort((a, b) => {
            return a.name.localeCompare(b.name);
          });
        } else {
          return cards.sort((a, b) => {
            return b.name.localeCompare(a.name);
          });
        }
      case 'quantity':
        return cards.sort((a, b) => {
          if (a.quantity == b.quantity) {
            return a.name.localeCompare(b.name);
          }
          if (direction == 'asc') {
            return a.quantity - b.quantity;
          } else {
            return b.quantity - a.quantity;
          }
        });
      case 'foil':
        if (direction == 'asc') {
          return cards.sort((a, b) => {
            if (!b.foil && a.foil) {
              return -1;
            } else if (!a.foil && b.foil) {
              return 1;
            } else {
              return a.name.localeCompare(b.name);
            }
          });
        } else {
          return cards.sort((a, b) => {
            if (!a.foil && b.foil) {
              return -1;
            } else if (!b.foil && a.foil) {
              return 1;
            } else {
              return a.name.localeCompare(b.name);
            }
          });
        }
      case 'set':
        return cards.sort((a, b) => {
          if (typeof a.expansion == 'undefined' && typeof b.expansion != 'undefined') {
            return direction == 'asc' ? -1 : 1;
          } else if (typeof b.expansion == 'undefined' && typeof a.expansion != 'undefined') {
            return direction == 'asc' ? 1 : -1;
          } else if (typeof a.expansion == 'undefined' && typeof b.expansion == 'undefined') {
            return a.name.localeCompare(b.name);
          } else if (a.expansion.name == b.expansion.name) {
            return a.name.localeCompare(b.name);
          }
          if (direction == 'asc') {
            return a.expansion.name.localeCompare(b.expansion.name);
          } else {
            return b.expansion.name.localeCompare(a.expansion.name);
          }
        });
      case 'color':
        return cards.sort((a, b) => {
          let aColor = a.color;
          let bColor = b.color;
          if (typeof aColor == 'undefined') aColor = '';
          if (typeof bColor == 'undefined') bColor = '';
          if (aColor == '' && bColor != '') {
            return direction == 'asc' ? 1 : -1;
          } else if (bColor == '' && aColor != '') {
            return direction == 'asc' ? -1 : 1;
          } else if (aColor == '' && bColor == '') {
            return a.name.localeCompare(b.name);
          } else if (aColor == bColor) {
            return a.name.localeCompare(b.name);
          } else if (typeof colorRank[aColor] == 'undefined' && typeof colorRank[bColor] != 'undefined') {
            return direction == 'asc' ? 1 : -1;
          } else if (typeof colorRank[bColor] == 'undefined' && typeof colorRank[aColor] != 'undefined') {
            return direction == 'asc' ? -1 : 1;
          } else if (typeof colorRank[aColor] == 'undefined' && typeof colorRank[bColor] == 'undefined') {
            return a.name.localeCompare(b.name);
          }
          if (direction == 'asc') {
            return colorRank[aColor] - colorRank[bColor];
          } else {
            return colorRank[bColor] - colorRank[aColor];
          }
        });
      case 'rarity':
        return cards.sort((a, b) => {
          let aRarity = a.rarity;
          let bRarity = b.rarity;
          if (typeof aRarity == 'undefined') aRarity = '';
          if (typeof bRarity == 'undefined') bRarity = '';
          if (aRarity == '' && bRarity != '') {
            return direction == 'asc' ? -1 : 1;
          } else if (bRarity == '' && aRarity != '') {
            return direction == 'asc' ? 1 : -1;
          } else if (aRarity == '' && bRarity == '') {
            return a.name.localeCompare(b.name);
          } else if (aRarity == bRarity) {
            return a.name.localeCompare(b.name);
          }
          if (direction == 'asc') {
            return rarityRank[aRarity] - rarityRank[bRarity];
          } else {
            return rarityRank[bRarity] - rarityRank[aRarity];
          }
        });
    }
    return cards; // Unknown sort type;
  }
  getDefaultDirection(sortType) {
    switch (sortType) {
      case 'quantity':
        return 'desc';
    }
    return 'asc';
  }
  handleSort(field) {
    let direction;
    if (this.state.sortField == field) {
      // Sort in the other order
      direction = (this.state.sortDirection == 'asc')? 'desc' : 'asc';
    } else {
      direction = this.getDefaultDirection(field);
    }
    let reSorted = this.sortCards(this.state.cards, field, direction);

    this.setState({
      sortField: field,
      sortDirection: direction,
      cards: reSorted
    });
  }
  getNextPage(currentPage, pageSize, recordCount) {
    let newPage = currentPage+1;
    if ((newPage-1) * pageSize > recordCount) {
      return currentPage;
    }
    return newPage;
  }
  pageNext() {
    let newPage = this.getNextPage(
      this.state.currentPage,
      this.state.pageSize,
      this.state.cards.length
    );
    if (newPage == this.state.currentPage) return;
    this.setState({
      currentPage: newPage
    });
  }
  getPreviousPage(currentPage) {
    let newPage = currentPage-1;
    if (newPage < 1) return currentPage;
    return newPage;
  }
  pagePrevious() {
    let newPage = this.getPreviousPage(this.state.currentPage);
    if (newPage == this.state.currentPage) return;
    this.setState({
      currentPage: newPage
    });
  }
  getCurrentPageRange(currentPage, pageSize) {
    let startAt = (currentPage - 1) * pageSize;
    let endAt = startAt + pageSize;
    return [startAt, endAt];
  }
  render() {
    let myId;
    if (typeof this.props.id !== 'undefined') {
      myId = this.props.id;
    }

    let pageRange = this.getCurrentPageRange(
      this.state.currentPage,
      this.state.pageSize
    );
    let currentCards = this.state.cards.slice(pageRange[0], pageRange[1]);

    let cardCount = dbUtil.countCards(this.state.cards);
    let startAt = pageRange[0];
    let shownEnd = (pageRange[1] > this.state.cards.length)? this.state.cards.length : pageRange[1];

    let pagination;
    if (this.props.pagination === false) {
      pagination = (
        <p className="pagination">
          {this.state.cards.length} records; {cardCount} total cards
        </p>
      );
    } else {
      pagination = (
        <p className="pagination">
          <button className="mini" onClick={this.pagePrevious}>&lt;</button>
          {startAt+1} through {shownEnd} of {this.state.cards.length} records; {cardCount} total cards
          <button className="mini" onClick={this.pageNext}>&gt;</button>
        </p>
      );
    }

    let columns;
    if (typeof this.props.columns == 'undefined') {
      columns = this.defaultColumns;
    } else {
      columns = this.props.columns;
    }
    let cardRowHandler;
    if (typeof this.props.cardRowHandler == 'function') {
      cardRowHandler = this.props.cardRowHandler;
    } else {
      cardRowHandler = function(card, index) {
        let key = typeof card._id == 'undefined' ? index : card._id;
        return (
          <rows.CardViewRow
            key={key}
            card={card}
            columns={columns}
          />
        );
      }
    }

    return (
      <div id={myId} className="card-table">
      <div className="content">
        <div className="controls" style={{ textAlign: 'right', paddingBottom: '0.25em' }}>
          <div className="search">
            <input
              type="text"
              placeholder="Filter..."
              style={{ width: '30em' }}
              onChange={this.handleFilter}
            />
          </div>
        </div>
        <table className="grid" cellPadding="0" cellSpacing="0">
          <thead>
            <rows.CardHeaderRow
              columns={columns}
              handleSort={this.handleSort}
            />
          </thead>
          <tbody>
            {currentCards.map(cardRowHandler)}
          </tbody>
        </table>
        {pagination}
      </div></div>
    );
  }
}

export default CardTable;
