import React from 'react';
import formatter from 'lib/formatter';
import mtgSets from 'lib/mtgSets';
import dbUtil from './lib/dbUtil';
import radio from 'radio';

const db = dbUtil.connect();

/**
 * A generic class for rendering a table cell with content.
 *
 * Why have a class that's just a table cell? So that in lists/filters, it's
 * possible to do a `thing.type == TableCell` comparison to see if it's already
 * ready to be inserted into the DOM. Otherwise, the `type` would be a DOMNode
 * type, and more error-checking would be needed to determine if it's a td/th
 * or some other type of node.
 */
const TableCell = function(props) {
  let content;
  if (typeof props.content == 'function') {
    content = props.content(props);
  } else if (typeof props.content !== 'undefined') {
    content = props.content;
  } else {
    content = props.children;
  }
  if (props.isHeader) {
    return (
      <th
        {...props}
      >
        {content}
      </th>
    );
  }
  return (
    <td
      {...props}
    >
      {content}
    </td>
  );
};

function colorLabel(color) {
  if (color == '') return null;
  let colorLower = color.toLowerCase();
  const colorMap = {
    white: <i className="ms ms-cost ms-w" />,
    blue: <i className="ms ms-cost ms-u" />,
    black: <i className="ms ms-cost ms-b" />,
    red: <i className="ms ms-cost ms-r" />,
    green: <i className="ms ms-cost ms-g" />,
    'white/blue': <i className="ms ms-cost ms-wu ms-split" />,
    'blue/black': <i className="ms ms-cost ms-ub ms-split" />,
    'black/red': <i className="ms ms-cost ms-br ms-split" />,
    'red/green': <i className="ms ms-cost ms-rg ms-split" />,
    'white/green': <i className="ms ms-cost ms-gw ms-split" />,
    'white/black': <i className="ms ms-cost ms-wb ms-split" />,
    'blue/red': <i className="ms ms-cost ms-ur ms-split" />,
    'black/green': <i className="ms ms-cost ms-bg ms-split" />,
    'white/red': <i className="ms ms-cost ms-rw ms-split" />,
    'blue/green': <i className="ms ms-cost ms-gw ms-split" />
  };
  const clanMap = {
    'white/blue': [<i className="ms ms-guild-azorius" />, ' Azorius'],
    'blue/black': [<i className="ms ms-guild-dimir" />, ' Dimir'],
    'black/red': [<i className="ms ms-guild-rakdos" />, ' Rakdos'],
    'red/green': [<i className="ms ms-guild-gruul" />, ' Gruul'],
    'white/green': [<i className="ms ms-guild-selesnya" />, ' Selesnya'],
    'white/black': [<i className="ms ms-guild-orzhov" />, ' Orzhov'],
    'blue/red': [<i className="ms ms-guild-izzet" />, ' Izzet'],
    'black/green': [<i className="ms ms-guild-golgari" />, ' Golgari'],
    'white/red': [<i className="ms ms-guild-boros" />, ' Boros'],
    'blue/green': [<i className="ms ms-guild-simic" />, ' Simic'],
  }

  if (typeof colorMap[colorLower] != 'undefined') {
    // Single Icon
    if (typeof clanMap[colorLower] != 'undefined') {
      // Two-color clan
      let base = [colorMap[colorLower]];
      base.push(' ');
      base = base.concat(clanMap[colorLower]);
      return base;
    }
    return colorMap[colorLower];
  }
  // Make list of icons
  let colors = color.split('/');
  return colors.filter(color => {
    return typeof colorMap[color.toLowerCase()] != 'undefined';
  }).map(color => {
    return colorMap[color.toLowerCase()];
  });
}

/**
 * Display a table cell with information from a card model.
 *
 * Props should contain `card` with the card model,
 * and `field` with the field to render.
 *
 * This class contains special logic for different fields to render
 * not as a literal representation of themselves
 * (e.g. Expansion and Foil flag).
 */
const CardFieldCell = function(props) {
  let card = props.card;
  let field = props.field;
  switch (field) {
    case 'quantity': {
      return (
        <TableCell style={{ textAlign: 'right' }}>
          {card.quantity}
        </TableCell>
      );
    }
    case 'foil': {
      return (
        <TableCell style={{ textAlign: 'center' }}>
          {card.foil? 'Y' : ''}
        </TableCell>
      );
    }
    case 'expansion': {
      if (typeof card.expansion == 'undefined') {
        return (
          <TableCell />
        );
      }
      return (
        <TableCell>
          {card.expansion.name} ({card.expansion.code}) <i className={"ss ss-" + card.expansion.code.toLowerCase()} />
        </TableCell>
      );
    }
    case 'color': {
      if (typeof card.color == 'undefined') {
        return (
          <TableCell />
        );
      }

      return (
        <TableCell>
          {colorLabel(card.color)}
        </TableCell>
      );
    }
    default: {
      return (
        <TableCell>{card[field]}</TableCell>
      );
    }
  }
};

class CardHeaderCell extends React.Component {
  constructor(props) {
    super(props);
    this.sortByQuantity = this.sortByQuantity.bind(this);
    this.sortByName = this.sortByName.bind(this);
    this.sortByFoil = this.sortByFoil.bind(this);
    this.sortBySet = this.sortBySet.bind(this);
    this.sortByColor = this.sortByColor.bind(this);
    this.sortByRarity = this.sortByRarity.bind(this);
  }
  sortByQuantity() {
    this.props.handleSort('quantity');
  }
  sortByName() {
    this.props.handleSort('name');
  }
  sortByFoil() {
    this.props.handleSort('foil');
  }
  sortBySet() {
    this.props.handleSort('set');
  }
  sortByColor() {
    this.props.handleSort('color');
  }
  sortByRarity() {
    this.props.handleSort('rarity');
  }
  render() {
    let field = this.props.field;
    switch (field) {
      case 'quantity': {
        return (
          <TableCell
            isHeader={true}
            style={{ width: '20px', cursor: 'pointer' }}
            onClick={this.sortByQuantity}
          >#</TableCell>
        );
      }
      case 'name': {
        return (
          <TableCell
            isHeader={true}
            style={{ cursor: 'pointer' }}
            onClick={this.sortByName}
          >Card</TableCell>
        );
      }
      case 'foil': {
        return (
          <TableCell
            isHeader={true}
            style={{ width: '20px', cursor: 'pointer' }}
            onClick={this.sortByFoil}
          >Foil</TableCell>
        );
      }
      case 'expansion': {
        return (
          <TableCell
            isHeader={true}
            style={{ cursor: 'pointer' }}
            onClick={this.sortBySet}
          >Expansion</TableCell>
        );
      }
      case 'color': {
        return (
          <TableCell
            isHeader={true}
            style={{ cursor: 'pointer' }}
            onClick={this.sortByColor}
          >Color</TableCell>
        );
      }
      case 'rarity': {
        return (
          <TableCell
            isHeader={true}
            style={{ cursor: 'pointer' }}
            onClick={this.sortByRarity}
          >Rarity</TableCell>
        );
      }
    }
  }
}

/**
 * A generic class for rendering a table row with cells.
 *
 * `cells` property should be an array of components
 * that render as table cells.
 */
class TableRow extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }
  handleClick(e) {
    if (typeof this.props.onClick == 'function') {
      this.props.onClick(e, this.props);
    }
  }
  render() {
    return (
      <tr
        {...this.props}
        onClick={this.handleClick}
      >
        {this.props.children}
      </tr>
    );
  }
}

class CardHeaderRow  extends React.Component {
  render() {
    let props = this.props;
    let cells = this.props.columns.map((col, index) => {
      if (typeof col == 'string') {
        // Standard Header cell
        return (
          <CardHeaderCell
            key={index}
            field={col}
            handleSort={props.handleSort}
          />
        );
      } else {
        if (col.type == TableCell) {
          // Already a table cell; just use it
          return col;
        }
        return (
          <TableCell
            key={index}
            isHeader={true}
          >
            {col}
          </TableCell>
        );
      }
    });
    return (
      <TableRow {...props}>{cells}</TableRow>
    )
  }
}

/**
 * Table row that shows static information about a card.
 */
class CardViewRow extends React.Component {
  render() {
    let props = this.props;
    let cells = this.props.columns.map((col, index) => {
      if (typeof col == 'string') {
        // Field name of card
        return (
          <CardFieldCell
            key={index}
            card={props.card}
            field={col}
          />
        );
      } else {
        if (col.type == TableCell) {
          // Already a table cell; just use it
          return col;
        }
        return (
          <TableCell key={index}>
            {col}
          </TableCell>
        );
      }
    });
    return (
      <TableRow id={props.card._id} {...props}>{cells}</TableRow>
    );
  }
}

class CardEditRow extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      editMode: false,
      card: this.props.card,
      isDirty: false
    };
    this.handleCancel = this.handleCancel.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.renderEditable = this.renderEditable.bind(this);
    this.renderStatic = this.renderStatic.bind(this);
  }
  componentDidMount() {
    radio('cancelOut').subscribe(this.handleCancel);
  }
  componentWillUnmount() {
    radio('cancelOut').unsubscribe(this.handleCancel);
  }
  handleCancel(e) {
    if (!this.state.editMode) return; // Not editing; nothing to do
    this.handleSave();
  }
  handleClick(e) {
    if (this.state.editMode) {
      // Currently editing, don't change anything
      e.stopPropagation();
      return;
    }
    this.setState({
      editMode: true
    });
  }
  handleChange(e) {
    let card = this.state.card;
    if (e.target.name == 'expansion') {
      // Save code and name of expansion
      if (e.target.value == 'none') {
        if (typeof card.expansion == 'undefined') return; // No change
        delete card.expansion;
      } else {
        if (typeof card.expansion != 'undefined'  && card.expansion.code == e.target.value) return; // No change
        card.expansion = {
          code: e.target.value,
          name: mtgSets.codeToName[e.target.value]
        };
      }
    } else if (e.target.name == 'foil') {
      // Translate foil check into boolean
      if (card.foil == e.target.checked) return; // No change
      card.foil = e.target.checked;
    } else if (e.target.name == 'quantity') {
      // Translate quantity into integer
      if (parseInt(e.target.value) == card.quantity) return; // No change
      card.quantity = parseInt(e.target.value);
    } else {
      if (card[e.target.name] == e.target.value) return; // No change
      card[e.target.name] = e.target.value;
    }
    this.setState({
      card: card,
      isDirty: true
    });
  }
  handleSave() {
    if (!this.state.isDirty) {
      // Nothing to change
      this.setState({
        editMode: false
      });
      return;
    }
    if (this.state.card.quantity == '' || this.state.card.quantity <= 0) {
      // Remove this card from the database
      db.remove(this.state.card).then(rs => {
        console.log('Card deleted', rs);
        this.setState(curState => {
          curState.editMode = false;
          curState.card._rev = rs.rev;
          curState.isDirty = false;
          if (typeof this.props.onDelete == 'function') {
            this.props.onDelete(curState.card); // Tell parent to update overall card listing
          }
          return curState;
        });
      }).catch(err => {
        console.error(err);
      });
    } else {
      this.state.card = dbUtil.setColorRarity(this.state.card);
      db.put(this.state.card).then(rs => {
        console.log('Card updated', rs);
        this.setState(curState => {
          curState.editMode = false;
          curState.card._rev = rs.rev;
          curState.isDirty = false;
          if (typeof this.props.onSave == 'function') {
            this.props.onSave(curState.card); // Tell parent to update overall card listing
          }
          return curState;
        });
      }).catch(err => {
        console.error(err);
      });
    }
  }
  render() {
    return this.state.editMode? this.renderEditable() : this.renderStatic();
  }
  renderEditable() {
    let card = this.state.card;
    let sets = formatter.setsWithCard(card.name, mtgSets.setReference);
    let setOptions = formatter.setOptions(sets);
    let defaultSet = typeof card.expansion != 'undefined' ? card.expansion.code : false;
    let foilCheck;
    if (card.foil) {
      foilCheck = <input type="checkbox" name="foil" value="Y" onChange={this.handleChange} checked="checked" />;
    } else {
      foilCheck = <input type="checkbox" name="foil" value="Y" onChange={this.handleChange} />;
    }

    let columns = [
      <input
        type="number"
        name="quantity"
        onChange={this.handleChange}
        value={card.quantity}
        style={{ width: '3em', textAlign: 'right' }}
      />,
      'name',
      foilCheck,
      <TableCell key="expansion" colSpan="3">
        <select
          name="expansion"
          onChange={this.handleChange}
          defaultValue={defaultSet}>
          <option value="none">None</option>
          {formatter.interleave(setOptions, "\n")}
        </select>
      </TableCell>
    ];

    return (
      <CardViewRow
        card={card}
        columns={columns}
        onClick={this.handleClick}
      />
    );
  }
  renderStatic() {
    let columns = [
      'quantity',
      'name',
      'foil',
      'expansion',
      'color',
      'rarity'
    ];
    return (
      <CardViewRow
        card={this.state.card}
        columns={columns}
        style={{ cursor: 'pointer' }}
        onClick={this.handleClick}
      />
    );
  }
}

class CardSelectRow extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }
  handleClick(e) {
    this.props.handleSelect(this.props.card._id);
  }
  render() {
    let checked = this.props.isChecked ? '\u2713' : '';
    let className = this.props.isChecked ? 'checked' : 'unchecked';

    let columns = [
      <TableCell key="checked">{checked}</TableCell>,
      'quantity',
      'name',
      'foil',
      'expansion',
      'color',
      'rarity'
    ];
    return (
      <CardViewRow
        card={this.props.card}
        columns={columns}
        className={className}
        style={{ cursor: 'pointer' }}
        onClick={this.handleClick}
      />
    );
  }
}

export default {
  TableCell,
  CardViewRow,
  CardHeaderRow,
  CardEditRow,
  CardSelectRow
};
