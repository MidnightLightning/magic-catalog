import React from 'react';

const NavView = function(props) {
  let pages = [
    {
      id: 'inventory',
      label: 'Inventory'
    },
    {
      id: 'mass-edit',
      label: 'Mass Edit'
    },
    {
      id: 'set-edit',
      label: 'Set Edit'
    },
    {
      id: 'conversion',
      label: 'Decklist conversion'
    }
  ];
  return (
    <nav>
      <ul>
        {
          pages.map(page => {
            let className = '';
            if (page.id == props.current) {
              className = 'active';
            }
            return (
              <li
                onClick={props.changePage}
                id={page.id}
                key={page.id}
                className={className}
              >{page.label}</li>
            );
          })
        }
      </ul>
    </nav>
  );
};

export default NavView;
