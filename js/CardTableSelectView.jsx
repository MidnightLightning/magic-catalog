import React from 'react';
import CardTable from './CardTable';
import rows from './CardTableRow';

class CardTableSelectView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedCards: {},
    };

    this.handleSelect = this.handleSelect.bind(this);
    this.clearSelection = this.clearSelection.bind(this);
    this.cardRowHandler = this.cardRowHandler.bind(this);
  }
  handleSelect(card) {
    let parentListener = this.props.handleUpdate;
    this.setState(curState => {
      let selected = curState.selectedCards;
      if (typeof selected[card] == 'undefined') {
        selected[card] = true;
      } else {
        delete(selected[card]);
      }
      curState.selectedCards = selected;
      if (typeof parentListener == 'function') {
        parentListener(selected);
      }
      return curState;
    });
  }
  clearSelection() {
    let parentListener = this.props.handleUpdate;
    this.setState(curState => {
      curState.selectedCards = {};
      if (typeof parentListener == 'function') {
        parentListener({});
      }
      return curState;
    });
  }
  cardRowHandler(card) {
    let selectedCards = this.state.selectedCards;
    let isChecked = selectedCards[card._id] === true;
    return (
      <rows.CardSelectRow
        key={card._id}
        card={card}
        isChecked={isChecked}
        handleSelect={this.handleSelect}
      />
    );
  }
  render() {
    let myId;
    if (typeof this.props.id !== 'undefined') {
      myId = this.props.id;
    }

    let selectedCards = this.state.selectedCards;
    let someSelected = Object.keys(selectedCards).length > 0;
    let deselect = someSelected ? '\u2713' : '';

    let columns = [
      <rows.TableCell
        isHeader={true}
        style={{ cursor: 'pointer', width: '2em' }}
        onClick={this.clearSelection}
      >{deselect}</rows.TableCell>,
      'quantity',
      'name',
      'foil',
      'expansion',
      'color',
      'rarity'
    ];
    return (
      <CardTable
        id={myId}
        cards={this.props.cards}
        pageSize={this.props.pageSize}
        columns={columns}
        cardRowHandler={this.cardRowHandler}
      />
    );
  }
}

export default CardTableSelectView;
