import React from 'react';

const SectionView = function(props) {
  return (
    <section>
    <h1>{props.title}</h1>
    </section>
  );
};

export default SectionView;
