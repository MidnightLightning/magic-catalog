import React from 'react';
import radio from 'radio';
import NavView from './NavView';
import InventoryView from './InventoryView';
import MassEditView from './MassEditView';
import SetEditView from './SetEditView';
import ConversionView from './ConversionView';

class AppView extends React.Component {
  constructor(props) {
    super(props);

    let jumpTo = window.location.hash.substr(1);
    if (jumpTo == '') {
      jumpTo = 'inventory';
      window.location.hash = jumpTo;
    }
    this.state = {
      currentPage: jumpTo
    };
    this.handlePageChange = this.handlePageChange.bind(this);
  }
  componentDidMount() {
    document.addEventListener('keydown', this.handleKey);
  }
  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleKey);
  }
  handlePageChange(e) {
    let newPage = e.target.id;
    if (newPage == this.state.currentPage) return;
    window.location.hash = newPage;
    this.setState({
      currentPage: newPage
    });
  }
  handleClick(e) {
    radio('cancelOut').broadcast(e);
  }
  handleKey(e) {
    if (e.keyCode === 27) {
      radio('cancelOut').broadcast(e);
    }
  }
  render() {
    this.props.db.info().then(info => {
      console.log(info);
    });

    let pageContent;
    switch (this.state.currentPage) {
      case 'conversion':
        pageContent = <ConversionView db={this.props.db} />;
        break;
      case 'mass-edit':
        pageContent = <MassEditView db={this.props.db} />;
        break;
      case 'set-edit':
        pageContent = <SetEditView db={this.props.db} />;
        break;
      default: // Inventory
        pageContent = <InventoryView db={this.props.db} />;
        break;
    }
    return (
      <div id="app-container" onClick={this.handleClick}>
        <NavView
          current={this.state.currentPage}
          changePage={this.handlePageChange}
        />
        {pageContent}
      </div>
    );
  }
}

export default AppView;
