import React from 'react';
import CardTable from './CardTable';
import dbUtil from './lib/dbUtil';
import rows from './CardTableRow';
import radio from 'radio';

const db = dbUtil.connect();

class CardTableEditView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      popupCard: false,
      popupCardlist: [],
      popupCardPosition: {}
    };

    this.columns = [
      'quantity',
      'name',
      'foil',
      'expansion',
      'color',
      'rarity'
    ];
    this.handleCancel = this.handleCancel.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleAdd = this.handleAdd.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.cardRowHandler = this.cardRowHandler.bind(this);
    this.popupCardRowHandler = this.popupCardRowHandler.bind(this);
  }
  componentDidMount() {
    radio('cancelOut').subscribe(this.handleCancel);
  }
  componentWillUnmount() {
    radio('cancelOut').unsubscribe(this.handleCancel);
  }
  handleCancel(e) {
    this.setState(curState => {
      curState.popupCard = false;
      return curState;
    });
  }
  backgroundClick(e) {
    e.stopPropagation(); // Stop here, to prevent dismissing the popup
  }
  handleClick(e, props) {
    let cardName = props.card.name;
    let self = this;

    function getRow(child) {
      if (child.tagName.toLowerCase() == 'tr') {
        return child;
      } else {
        return getRow(child.parentElement);
      }
    }

    let thisRow = getRow(e.target);
    db.query('magic-collection/cardsByNameSet', {
      startkey: [cardName],
      endkey: [cardName, {}],
      include_docs: true
    }).then(rs => {
      rs.rows = rs.rows.map(record => record.doc);
      self.setState({
        popupCard: props.card,
        popupCardlist: rs.rows,
        popupCardPosition: thisRow.getBoundingClientRect()
      });
    });
  }
  handleAdd(e) {
    let cardName = this.state.popupCard.name;
    db.post({
      type: 'card',
      name: cardName,
      quantity: 1
    }).then(rs => {
      this.setState(curState => {
        let newCard = {
          _id: rs.id,
          _rev: rs.rev,
          name: cardName,
          quantity: 1,
          type: 'card'
        };
        curState.popupCardlist.push(newCard);
        if (typeof this.props.onAdd === 'function') {
          this.props.onAdd(newCard);
        }
        return curState;
      });
    });
  }
  handleDelete(card) {
    this.setState(curState => {
      curState.popupCardlist = curState.popupCardlist.filter(record => {
        return record._id != card._id;
      });
      if (typeof this.props.onDelete === 'function') {
        this.props.onDelete(card); // Tell parent to update overall card listing
      }
      return curState;
    });
  }
  cardRowHandler(card) {
    return (
      <rows.CardViewRow
        key={card._id}
        card={card}
        columns={this.props.columns}
        style={{ cursor: 'pointer' }}
        onClick={this.handleClick}
      />
    );
  }
  popupCardRowHandler(card) {
    return (
      <rows.CardEditRow
        key={card._id}
        card={card}
        columns={this.columns}
        onDelete={this.handleDelete}
        onSave={this.props.onSave}
      />
    );
  }
  render() {
    let popup = <div className="popup"></div>;
    if (this.state.popupCard !== false) {
      // Show popup for this card
      let card = this.state.popupCard;
      let popupStyle = {
        left: this.state.popupCardPosition.left + window.scrollX + 10,
        top: this.state.popupCardPosition.top + window.scrollY + 3
      };
      // TODO: If height of popup is taller than page, fix background display
      popup = (
        <div className="popup active" style={popupStyle} onClick={this.backgroundClick}>
          <h3>{card.name}</h3>
          <CardTable
            cards={this.state.popupCardlist}
            columns={this.columns}
            cardRowHandler={this.popupCardRowHandler}
            pagination={false}
          />
          <button onClick={this.handleAdd}>Add Record</button>
        </div>
      );
    }

    return (
      <div className="popup-container">
        <CardTable
          cardRowHandler={this.cardRowHandler}
          {...this.props}
        />
        {popup}
      </div>
    );
  }
}

export default CardTableEditView;
