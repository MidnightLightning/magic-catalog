import React from 'react';
import formatter from 'lib/formatter';
import dbUtil from 'lib/dbUtil';
import mtgSets from 'lib/mtgSets';
import mtgCards from 'lib/mtgCards';
import CardTableEditView from 'CardTableEditView';

const db = dbUtil.connect();

class SetEditView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentSet: false,
      inventory: []
    };

    this.columns = [
      'quantity',
      'name',
      'expansion',
      'color',
      'rarity'
    ];
    this.pickSet = this.pickSet.bind(this);
    this.updateListing = this.updateListing.bind(this);
    this.handleAdd = this.handleAdd.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleSave = this.handleSave.bind(this);
  }
  pickSet(e) {
    if (e.target.value == 'none') return;
    this.updateListing(e.target.value);
  }
  updateListing(setCode) {
    // Start with all cards in the set
    let currentSet = {
      code: setCode,
      name: mtgSets.codeToName[setCode]
    };
    let baseCards = {};
    mtgCards.bySet[currentSet.code].map(card => {
      card.expansion = currentSet;
      card.quantity = 0;
      baseCards[card.name] = card;
    });

    // Now find any inventory items that match
    db.query('magic-collection/cardsByNameSet', {
      include_docs: true
    }).then(rs => {
      rs.rows.map(record => {
        let card = record.doc;
        if (typeof baseCards[card.name] != 'undefined') {
          baseCards[card.name].quantity += parseInt(card.quantity);
        }
      });

      let final = [];
      Object.keys(baseCards).map(key => {
        final.push(baseCards[key]);
      });

      this.setState({
        currentSet: setCode,
        inventory: final
      });
    });
  }
  handleAdd(card) {
    this.updateListing(this.state.currentSet);
  }
  handleDelete(card) {
    this.updateListing(this.state.currentSet);
  }
  handleSave(card) {
    this.updateListing(this.state.currentSet);
  }
  render() {
    let currentSet;
    if (this.state.currentSet !== false) {
      currentSet = {
        code: this.state.currentSet,
        name: mtgSets.codeToName[this.state.currentSet]
      };
    }
    let setOptions = formatter.setOptions(mtgSets.setReference, currentSet);
    let setListing;
    if (typeof currentSet != 'undefined') {
      setListing = (
        <CardTableEditView
          id="setCards"
          cards={this.state.inventory}
          columns={this.columns}
          onAdd={this.handleAdd}
          onDelete={this.handleDelete}
          onSave={this.handleSave}
          pageSize="50"
        />
      );
    }

    return (
      <section>
        <h1>Edit by Sets</h1>
        <p>Select a set to list all cards in that set, and adjust quantities</p>
        <p><select name="expansion" onChange={this.pickSet}>
          <option value="none">--</option>
          {setOptions}
        </select></p>
        {setListing}
      </section>
    );
  }
}

export default SetEditView;
