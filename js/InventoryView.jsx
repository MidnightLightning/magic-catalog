import React from 'react';
import CardTableEditView from 'CardTableEditView';
import conversion from 'lib/conversion';
import dbUtil from 'lib/dbUtil';
import lex from 'lib/lex';

const db = dbUtil.connect();

class InventoryView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inventory: false,
      errMsg: false
    };

    this.columns = [
      'quantity',
      'name',
      'color'
    ];
    this.updateCollection = this.updateCollection.bind(this);
    this.handleAdd = this.handleAdd.bind(this);
    this.handleAddList = this.handleAddList.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleSave = this.handleSave.bind(this);
  }
  componentDidMount() {
    this.updateCollection();
  }
  updateCollection() {
    db.query('magic-collection/cardsByNameSet', {
      include_docs: true
    }).then(rs => {
      let cards = rs.rows.map(record => record.doc);
      cards = dbUtil.mergeCards(cards);
      this.setState({
        inventory: cards
      });
    });
  }
  handleAddList(e) {
    let decklist = this.deckList.value;
    let tokens = lex(decklist);
    if (decklist.length == 0) return; // Nothing to parse
    //conversion.printTokens(lex(decklist));
    let cards = conversion.splitOnNewline(tokens).map(line => {
      return conversion.fromDecklist(line);
    }).filter(card => {
      return card !== false;
    });
    db.bulkDocs(cards.map(card => {
      card.type = 'card';
      return card;
    })).then(() => {
      this.updateCollection();
    }, err => {
      this.setState({
        errMsg: err.message
      });
    });
  }
  handleAdd(card) {
    this.updateCollection();
  }
  handleDelete(card) {
    this.updateCollection();
  }
  handleSave(card) {
    this.updateCollection();
  }
  render() {
    let inventoryList;
    if (this.state.inventory === false) {
      inventoryList = 'Loading...';
    } else {
      let cards = this.state.inventory;
      inventoryList = (
        <div className="inventory">
          <CardTableEditView
            cards={cards}
            columns={this.columns}
            onAdd={this.handleAdd}
            onDelete={this.handleDelete}
            onSave={this.handleSave}
          />
        </div>
      );
    }

    let errMsg;
    if (this.state.errMsg !== false) {
      errMsg = (
        <div className="alert alert-error">{this.state.errMsg}</div>
      );
    }

    return (
      <section>
        <h1>Magic Collection</h1>
        {errMsg}
        {inventoryList}
        <p>Enter some cards to add to your collection:</p>
        <div>
          <h2>Decklist format</h2>
          <p>Valid input formats:</p>
          <ul>
            <li><code>quantity name</code></li>
            <li><code>quantity name [edition]</code>: Card from specific edition</li>
            <li><code>quantity name (F)</code>: Foil card</li>
            <li><code>quantity name (F) [edition]</code>: Foil card from specific edition</li>
          </ul>
          <textarea
            className="code"
            ref={(ref) => this.deckList = ref}
            style={{ width: '30em', height: '10em' }}
          ></textarea><br />
          <button onClick={this.handleAddList}>Add</button>
        </div>
      </section>
    );
  }
}

export default InventoryView;
