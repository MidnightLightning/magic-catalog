import React from 'react';
import CardTableSelectView from 'CardTableSelectView';
import mtgSets from 'lib/mtgSets';
import dbUtil from './lib/dbUtil';

const db = dbUtil.connect();

class MassEditView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inventory: false,
      selectedCards: {},
      errMsg: false
    };

    this.updateCollection = this.updateCollection.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
    this.handleSelectionChange = this.handleSelectionChange(this);
  }
  componentDidMount() {
    this.updateCollection();
  }
  updateCollection() {
    db.query('magic-collection/cardsByNameSet', {
      include_docs: true
    }).then(rs => {
      rs.rows = rs.rows.map(record => record.doc);
      this.setState({
        inventory: rs
      });
    });
  }
  handleUpdate() {
    let foil = this.refs.foil.value;
    let set = this.refs.set.value;
    let cards = Object.keys(this.state.selectedCards);
    if (cards.length == 0) return; // No cards to edit
    let inventory = this.state.inventory.rows;
    let waitingFor = [];
    cards.map(cardId => {
      let card = dbUtil.findById(inventory, cardId);
      if (card === false) return; // No such card?
      let isDirty = false;
      if (foil != '--') {
        let isFoil = foil == 'Yes' ? true : false;
        if (card.foil != isFoil) {
          card.foil = isFoil;
          isDirty = true;
        }
      }
      if (set != '--') {
        if (typeof card.expansion == 'undefined' || card.expansion.code != set) {
          card.expansion = {
            code: set,
            name: mtgSets.codeToName[set]
          };
          // Update color and rarity
          card = dbUtil.setColorRarity(card);
          isDirty = true;
        }
      }

      if (!isDirty) return; // No changes
      waitingFor.push(db.put(card));
    });

    Promise.all(waitingFor).then(this.updateCollection);
  }
  handleSelectionChange(selectedCards) {
    this.setState({
      selectedCards: selectedCards
    });
  }
  render() {
    let inventoryList;
    if (this.state.inventory === false) {
      inventoryList = 'Loading...';
    } else {
      let cards = this.state.inventory.rows;
      inventoryList = (
        <div className="inventory">
          <CardTableSelectView
            cards={cards}
            pageSize="50"
            handleUpdate={this.handleSelectionChange}
          />
        </div>
      );
    }

    let errMsg;
    if (this.state.errMsg !== false) {
      errMsg = (
        <div className="alert alert-error">{this.state.errMsg}</div>
      );
    }

    let setOptions = mtgSets.setReference.map(set => {
      return (
        <option value={set.code}>{set.name} ({set.code})</option>
      );
    });

    return (
      <section>
        <h1>Mass Edit Collection</h1>
        <p>Select multiple records below, and edit their fields all at once:</p>
        {errMsg}
        <div style={{ marginBottom: '1em' }}>
          Foil:
            <select ref="foil">
              <option>--</option>
              <option>No</option>
              <option>Yes</option>
            </select>,
          Set:
            <select ref="set">
              <option>--</option>
              {setOptions}
            </select>
          <button className="mini" onClick={this.handleUpdate}>Update</button>
        </div>
        {inventoryList}
      </section>
    );
  }
}

export default MassEditView;
