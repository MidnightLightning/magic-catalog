import React from 'react';
import CardTable from 'CardTable';
import conversion from 'lib/conversion';
import dbUtil from 'lib/dbUtil';
import lex from 'lib/lex';

class ConversionView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      initialLoad: false,
      decklistText: 'Loading...',
      parsedCards: [],
      errMsg: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleParse = this.handleParse.bind(this);
  }
  componentDidMount() {
    this.props.db.get('conversion:lastInput').then(doc => {
      this.setState({
        initialLoad: true,
        decklistText: doc.text,
        errMsg: false
      });
    }, err => {
      if (err.status == 404) {
        // No default; not an issue
        this.setState({
          initialLoad: true,
          decklistText: ''
        });
        return;
      }
      // Some other error thrown
      this.setState({
        initialLoad: true,
        errMsg: err.message
      });
    });
  }
  handleChange(e) {
    this.setState({
      decklistText: e.target.value
    });
  }
  handleParse(e) {
    let decklist = this.deckList.value;
    let tokens = lex(decklist);
    if (decklist.length == 0) return; // Nothing to parse
    dbUtil.upsert(this.props.db, 'conversion:lastInput', { text: decklist }).catch(err => {
      this.setState({
        errMsg: err.message
      });
    });

    //conversion.printTokens(tokens);
    let cards = conversion.splitOnNewline(tokens).map(line => {
      let card = conversion.fromDecklist(line);
      return dbUtil.setColorRarity(card);
    }).filter(card => {
      return card !== false;
    });
    this.setState({
      parsedCards: cards
    });
  }
  render() {
    let disableInput = !this.state.initialLoad;
    let text = this.state.decklistText;
    let errMsg;
    if (this.state.errMsg !== false) {
      errMsg = (
        <div className="alert alert-error">{this.state.errMsg}</div>
      );
    }
    let parsedCardTable;
    if (this.state.parsedCards.length > 0) {
      parsedCardTable = (
        <CardTable
          id="parsedCards"
          cards={this.state.parsedCards}
        />
      );
    }

    return (
      <section>
        <h1>Decklist conversion</h1>
        <p>Tidy up your decklist text files, and convert to/from CSV files.</p>
        <div>
          <h2>Decklist format</h2>
          <p>Enter something resembling <code>quantity name</code> to tidy it up. If you have foils (<code>(F)</code>) or expansion sets (some text in brackets) indicated, this script will attempt to parse them out and standardize them.</p>
          {errMsg}
          <textarea
            className="code"
            ref={(ref) => this.deckList = ref}
            disabled={disableInput}
            onChange={this.handleChange}
            value={text}
            style={{ width: '30em', height: '20em' }}
          ></textarea><br />
          <button onClick={this.handleParse}>Parse</button>
          {parsedCardTable}
        </div>
      </section>
    );
  }
}

export default ConversionView;
