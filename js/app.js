import React from 'react';
import ReactDOM from 'react-dom';
import AppView from './AppView';
import dbUtil from './lib/dbUtil';

const db = dbUtil.connect();

// Set design document
let dDoc = {
  _id: '_design/magic-collection',
  views: {
    cards: {
      map: function(doc) {
        if (doc.type !== 'card') return;
        emit(doc._id);
      }.toString()
    },
    cardsByNameSet: {
      map: function(doc) {
        if (doc.type !== 'card') return;
        if (typeof doc.expansion != 'undefined') {
          emit([doc.name, doc.expansion.name]);
        } else {
          emit([doc.name, null]);
        }
      }.toString()
    },
    cardsBySet: {
      map: function(doc) {
        if (doc.type !== 'card') return;
        if (typeof doc.expansion == 'undefined') return;
        if (typeof doc.expansion.code != 'undefined') {
          emit(doc.expansion.code);
        }
        if (typeof doc.expansion.name != 'undefined') {
          emit(doc.expansion.name);
        }
      }.toString()
    }
  }
};

function alphabetizeProps(obj) {
  let props = Object.keys(obj).sort();
  let out = {};
  props.map(prop => {
    if (typeof obj[prop] == 'object') {
      out[prop] = alphabetizeProps(obj[prop]);
    } else {
      out[prop] = obj[prop];
    }
  });
  return out;
}

db.get('_design/magic-collection').then(doc => {
  dDoc._rev = doc._rev;
  if (JSON.stringify(alphabetizeProps(dDoc)) == JSON.stringify(alphabetizeProps(doc))) {
    return; // Already identical
  }
  return db.put(dDoc);
}, err => {
  if (err.status == 404) {
    // No current doc; just put
    return db.put(dDoc);
  }
  throw err; // Some other error; re-throw
}).catch(err => {
  console.log(err);
});

// Draw application
let appContainer = document.getElementById('app');
ReactDOM.render(
  React.createElement(AppView, {
    db: db
  }),
  appContainer
);
